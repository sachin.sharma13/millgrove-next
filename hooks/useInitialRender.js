import { useEffect, useState } from 'react';

const useInitialRender = () => {
  const [isInitialRender, setIsInitialRender] = useState(true);

  useEffect(() => setIsInitialRender(false), []);

  return [isInitialRender];
};

export { useInitialRender };
