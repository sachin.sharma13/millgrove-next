import { useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { LEFT_ARROW, RIGHT_ARROW } from '../utils/assets';
import { cloudFrontUrl } from '../utils/constants';

const accordionData = [
  {
    title: 'Facade',
    features: {
      Heath: [
        `${cloudFrontUrl}/facade_heath1.webp`,
        `${cloudFrontUrl}/facade_heath2.webp`,
      ],
      Meadow: [
        `${cloudFrontUrl}/facade_meadow1.webp`,
        `${cloudFrontUrl}/facade_meadow2.webp`,
      ],
      Orchard: [
        `${cloudFrontUrl}/facade_orchard1.webp`,
        `${cloudFrontUrl}/facade_orchard2.webp`,
      ],
    },
  },
  {
    title: 'Entrance',
    features: {
      'Front Entrance': [`${cloudFrontUrl}/entrance_front.webp`],
      'Side Entrance': [`${cloudFrontUrl}/entrance_side.webp`],
    },
  },
  {
    title: 'Size',
    features: {
      '3 FLOORS (>8000ft<sup>2</sup> &nbsp; CA)': [`${cloudFrontUrl}/8000ft.webp`],
      '2 FLOORS (>5000ft<sup>2</sup> &nbsp; CA)': [`${cloudFrontUrl}/5000ft.webp`],
    },
  },
  {
    title: 'Outdoors',
    features: {
      Pool: [`${cloudFrontUrl}/outdoors_pool.webp`],
      'Extended Garden': [`${cloudFrontUrl}/outdoors-garden.webp`],
    },
  },
];

export const AccordionCarouselMobile = ({ styles }) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [currentAccordion, setCurrentAccordion] = useState(0);
  const [currentIndex, setCurrentIndex] = useState(0);

  const { title, features } = accordionData[currentAccordion];
  const featuresKeys = Object.keys(features);

  return (
    <>
      <div className={styles['mobile-carousel-controls']}>
        <div className={styles['arrows-mobile']}>
          <span
            onClick={() => {
              setCurrentAccordion((prev) =>
                prev === 0 ? accordionData.length - 1 : prev - 1
              );
              setCurrentSlide(0);
              setCurrentIndex(0);
            }}
          >
            <LEFT_ARROW />
          </span>
          <div>{title}</div>
          <span
            onClick={() => {
              setCurrentAccordion((prev) =>
                prev === accordionData.length - 1 ? 0 : prev + 1
              );
              setCurrentSlide(0);
              setCurrentIndex(0);
            }}
          >
            <RIGHT_ARROW />
          </span>
        </div>
        <div className={styles['features']}>
          {featuresKeys.map((feat, index) => (
            <div
              key={index}
              style={
                index === currentSlide
                  ? { backgroundColor: '#ffffff', border: ' 3px solid #d2ac47' }
                  : undefined
              }
              onClick={() => setCurrentSlide(index)}
              dangerouslySetInnerHTML={{ __html: feat }} 
            >
            </div>
          ))}
        </div>
      </div>
      <div className={styles['images-div-mobile']}>
        <Carousel
          selectedItem={currentSlide}
          infiniteLoop={true}
          showThumbs={false}
          showIndicators={false}
          swipeable={false}
          showArrows={false}
          showStatus={false}
          goToSlide={setCurrentSlide}
          onChange={setCurrentSlide}
        >
          {featuresKeys.map((key, index) => (
            <div key={index}>
              <div className={styles['image-container']}>
                {currentIndex === 0 ? (
                  <img src={features[key][0]} alt="features-image" />
                ) : (
                  <img src={features[key][1]} alt="features-image" />
                )}
              </div>
              <div className={styles['dark-backdrop']}></div>
              <div className={styles['arrows']}>
                {features[key]?.length > 1 && (
                  <>
                    <div
                      onClick={() =>
                        setCurrentIndex(
                          currentIndex === features[key]?.length - 1
                            ? 0
                            : currentIndex + 1
                        )
                      }
                      className={styles['arrow-container']}
                      style={
                        currentIndex === 0 ? { display: 'none' } : undefined
                      }
                    >
                      <div>Front</div>
                      <LEFT_ARROW className={styles['left-arrow']} />
                    </div>
                    <div
                      onClick={() =>
                        setCurrentIndex(
                          currentIndex === 0
                            ? features[key]?.length - 1
                            : currentIndex - 1
                        )
                      }
                      style={
                        currentIndex !== 0 ? { display: 'none' } : undefined
                      }
                      className={styles['arrow-container']}
                    >
                      <RIGHT_ARROW className={styles['right-arrow']} />
                      <div>Rear</div>
                    </div>
                  </>
                )}
              </div>
            </div>
          ))}
        </Carousel>
      </div>
    </>
  );
};
