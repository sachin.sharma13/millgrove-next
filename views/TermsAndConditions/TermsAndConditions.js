import { useRouter } from 'next/router';
import React from 'react';
import { CloseIcon } from '../../public/icons/icons';
import styles from './TermsAndConditions.module.scss';

const TnC = () => {
  const router = useRouter();
  const isComingFromEmail = router.query.fromEmail;

  return (
    <div className={styles.pageWrapper}>
      <div className={styles.tncWrapper}>
        <div className={styles.content}>
          <h2 id="how-does-Millgrove-use-my-information">
            Terms and Conditions
          </h2>
          <p>
            Welcome to our website{' '}
            <strong
              className={styles['company-site']}
              onClick={() => router.push('/home')}
            >
              www.millgrove.in
            </strong>
          </p>
          <p>
            The information contained in this website is for general information
            purposes only. The information is provided by{' '}
            <strong onClick={() => router.push('/home')}></strong>, a property
            of TRL Riceland Private Limited (“Company”). The user is cautioned
            and advised not to rely upon any information stated on any other
            websites which may appear to be similar to the Company’s official
            website and/or contain Company’s logo/brand name or information
            about the Company.
          </p>
          <p>
            By using or accessing this website, the user agrees, acknowledges,
            and accepts all the terms and conditions of this disclaimer without
            any qualification or limitation.
          </p>
          <p>
            Information contained on this website may be changed or updated at
            any time without notice. To keep yourself updated on any changes
            introduced, please keep visiting the website and reviewing the terms
            and conditions.
          </p>
          <p>
            Queries/feedback/etc. are not monitored on the website, therefore
            shall not be construed as read or registered with Company or the
            owner of the website. This website may contain other proprietary
            notices and copyright information, the terms of which must be
            observed and followed.
          </p>
          <p>
            While we endeavour to keep the information up to date and correct,
            we make no representations or warranties of any kind, express or
            implied, about the completeness, accuracy, reliability, suitability,
            or availability with respect to the website or the information,
            products, services, or related graphics contained on the website for
            any purpose. Any reliance you place on such information is therefore
            strictly at your own risk.
          </p>
          <p>
            Nothing on this website constitutes advertising, marketing, booking,
            selling or an offer for sale, or invitation to purchase a unit in
            the project in any manner by the Company. This website does not
            constitute part of a legal contract. By visiting this website, the
            user agrees that the information including brochures and marketing
            collaterals on this website are solely for informational purposes
            only and the user has not relied on this information for making any
            booking/purchase a unit in the project of the Company.
          </p>
          <p>
            The content on this website, regarding the project and in respect of
            the plots and/or building, project layout, area, amenities,
            landscapes, other services, and other relevant terms, are based on
            the approved and sanctioned plans subject to any modification and
            change in consonance with relevant laws or as directed by the
            authorities. The visuals, pictures, images/renderings/maps of the
            project are purely indicative, informative, and representational in
            nature and only an artistic impression, unless specifically claimed
            to be actual photographs.
          </p>
          <p>
            You are requested to independently, either directly or through your
            legal/financial consultants, thoroughly verify all details/documents
            pertaining to this project as available on{' '}
            <a href="https://haryanarera.gov.in" target={'_blank'}>
              https://haryanarera.gov.in
            </a>{' '}
            (bearing HRERA registration number
            RC/REP/HARERA/GGM/696/428/2023/40) or at Company's office to
            understand the documents and information in all respect prior to
            concluding any decision for buying. The Company will not be
            accepting any bookings or allotments solely based on the images,
            material, stock photography, projections, details, and descriptions
            that are currently available and/or displayed on this website.
          </p>
          <p>
            This website and all its contents are provided on "as is" and on "as
            available" basis. No information given under this website creates a
            warranty or expand the scope of any warranty that cannot be
            disclaimed under applicable law. Your use of this website is solely
            at your own risk. This website is for guidance only. It does not
            constitute part of an offer or contract. The design & specifications
            are subject to change without prior notice. The computer-generated
            images are the artistic impression and are an indicative of the
            actual designs.
          </p>
          <p>
            You agree to defend, indemnify, and hold harmless the Company and/
            or its associate entities, their officers, directors, employees, and
            agents, from and against any claims, actions or demands, including
            without limitation reasonable legal and accounting fees, alleging,
            or resulting from your use of the website or your breach of these
            terms and conditions of this website. The Company expressly
            disclaims all liability in respect to actions taken or not taken
            based on any or all the contents of this website.
          </p>
          <p>
            This website may contain links to other websites including those
            operated and maintained by third parties. The Company includes these
            links solely as a convenience to you, and the presence of such a
            link does not imply a responsibility for the linked website or an
            endorsement of the linked website, its operator, or its contents.
            The Company makes no representations whatsoever about any other
            website which you may access through this one. When you access a
            non-Company website, even one that may contain the Company-logo,
            please note that it is independent from the Company, and that the
            Company has no control over the content on that website. It is up to
            you to take precautions to ensure that whatever you select for your
            use is free of such items as viruses, worms, trojan horses and other
            items of a destructive nature.
          </p>
          <p>
            In no event shall the Company be liable to any party for any direct,
            indirect, special, or other consequential damages for any use of
            this website, or any other hyperlinked website, including without
            limitation, any lost profits, business interruption, loss of
            programs or other data on your information handling system or
            otherwise, even if we are expressly advised of the possibility of
            such damages.
          </p>
          <p>
            The foregoing are subject to the prevailing laws of India and RERA
            authority and/or courts at Gurugram, Haryana and High Court of
            Punjab and Haryana, India shall have the exclusive jurisdiction on
            any dispute that may arise out of the use of this website.
          </p>
          <p>We thank you for your patience and understanding.</p>
        </div>
        {!isComingFromEmail ? (
          <div
            onClick={() => router.back()}
            className={styles.closeIconWrapper}
          >
            <CloseIcon color="#f9f6ed" height="100%" width="100%" />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default TnC;
