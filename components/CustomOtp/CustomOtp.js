import React, { useEffect, useRef } from 'react';
import { checkNonNumericInput } from '../../utils/checkNonNumericInput';
import styles from './CustomOtp.module.scss';

const CustomOtp = ({ otpLength = 4, otp, setOtp }) => {
  const inputRef = useRef();
  const inputArray = new Array(otpLength).fill(0);
  function handleChange(e) {
    const val = e.target.value;
    const idx = e.target.id.split('otp')[1];
    const newString = otp.slice(0, idx) + val + otp.slice(idx);
    if (val === '') {
      setOtp(
        (prev) => prev.slice(0, Number(idx)) + prev.slice(Number(idx) + 1)
      );
    } else {
      setOtp(newString);
    }
    const nextElementSibling = e.target.nextElementSibling;
    if (nextElementSibling && e.target.value !== '') {
      nextElementSibling.focus();
    }
  }
  function handleInputKeyDown(e) {
    checkNonNumericInput(e);
    if (e.key === 'ArrowLeft') {
      const prevElementSibling = e.target.previousElementSibling;
      if (prevElementSibling) {
        prevElementSibling.focus();
      }
    }
    if (e.key === 'ArrowRight') {
      const nextElementSibling = e.target.nextElementSibling;
      if (nextElementSibling && e.target.value !== '') {
        nextElementSibling.focus();
      }
    }
    if (e.key === 'Backspace' && e.target.value === '') {
      const prevElementSibling = e.target.previousElementSibling;
      if (prevElementSibling) {
        prevElementSibling.focus();
      }
    }
  }

  const getValue = (otp, idx) => {
    return otp.slice(idx, idx + 1);
  };

  useEffect(() => {
    if (otp === '') {
      inputRef.current.focus();
    }
  }, [otp]);
  return (
    <div className={styles['otp-wrapper']}>
      {inputArray.map((_, idx) => (
        <input
          value={getValue(otp, idx)}
          onChange={(e) => handleChange(e)}
          onKeyDown={handleInputKeyDown}
          ref={idx === 0 ? inputRef : null}
          key={idx}
          id={`otp${idx}`}
          inputMode="numeric"
          maxLength={1}
          className={styles['otp-input']}
        />
      ))}
    </div>
  );
};

export default CustomOtp;
