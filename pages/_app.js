import AOS from 'aos';
import { useEffect } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AuthProvider from '../context/AuthContext';
import '../styles/theme.scss';
import { useRouter } from 'next/router';
import gsap from 'gsap';

export const fixTimeoutTransition = (router, timeout) => {
  router.events.on('beforeHistoryChange', () => {
    // Create a clone of every <style> and <link> that currently affects the page. It doesn't matter
    // if Next.js is going to remove them or not since we are going to remove the copies ourselves
    // later on when the transition finishes.
    const nodes = document.querySelectorAll(
      'link[rel=stylesheet], style:not([media=x])'
    );
    const copies = [...nodes].map((el) => el.cloneNode(true));
    for (let copy of copies) {
      // Remove Next.js' data attributes so the copies are not removed from the DOM in the route
      // change process.
      copy.removeAttribute('data-n-p');
      copy.removeAttribute('data-n-href');
      copy.removeAttribute('data-n-css');
      copy.removeAttribute('data-n-g');

      // Add duplicated nodes to the DOM.
      document.head.appendChild(copy);
    }

    const handler = () => {
      // Emulate a `.once` method using `.on` and `.off`
      router.events.off('routeChangeComplete', handler);

      window.setTimeout(() => {
        for (let copy of copies) {
          // Remove previous page's styles after the transition has finalized.
          document.head.removeChild(copy);
        }
      }, timeout);
    };

    router.events.on('routeChangeComplete', handler);
  });
};
function MyApp({ Component, pageProps }) {
  const router = useRouter();

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  useEffect(() => {
    const TRANSITION_DURATION = 500;
    fixTimeoutTransition(router, TRANSITION_DURATION);
  }, []);

  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
  }, []);

  return (
    <AuthProvider>
      <Component {...pageProps} key={router.route} />
      <ToastContainer
        position="top-center"
        autoClose={4000}
        hideProgressBar={true}
      />
    </AuthProvider>
  );
}

export default MyApp;
