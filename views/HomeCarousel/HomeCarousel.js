/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router';
import { useRef } from 'react';
import { useEffect, useState } from 'react';
import { useIntersection } from '../../hooks/useIntersection';
import { sliderData } from '../../utils/assets';

function HomeCarousel() {
  const [elementInView, setElementInView] = useState(sliderData[0].classStyle);

  useEffect(() => {
    let controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 0,
        duration: 1300,
        offset: 0,
      },
    });

    let slides = document.querySelectorAll('section.slide-sec-carousel');
    for (let i = 0; i < slides.length; i++) {
      let animation = TweenMax.to(slides[i], 0.7, {
        opacity: 0,
        ease: Quad.easeInOut,
      });

      new ScrollMagic.Scene({
        triggerElement: slides[i],
      })
        .setPin(slides[i], { pushFollowers: false })
        .addTo(controller)
        .setTween(animation);
    }
  }, []);

  return (
    <div className="millglove-slider-section">
      <div className="homeCarousel-circle-container">
        {sliderData.map((item) => (
          <div
            key={item.title}
            className="homeCarousel-circle"
            style={{
              backgroundColor:
                elementInView === item.classStyle ? 'black' : 'transparent',
            }}
            // onClick={() =>document.getElementById(item.classStyle).scrollIntoView()}
          ></div>
        ))}
      </div>
      <div>
        {sliderData.slice(0, -1).map((item, index) => (
          <HomeCarouselSection
            key={item.title}
            item={item}
            index={index}
            setElementInView={setElementInView}
            lastSlide={false}
          />
        ))}
        <HomeCarouselSection
          item={sliderData.slice(-1)[0]}
          index={sliderData.length - 1}
          setElementInView={setElementInView}
          lastSlide={true}
        />
      </div>
    </div>
  );
}

export default HomeCarousel;

const HomeCarouselSection = ({ item, index, setElementInView, lastSlide }) => {
  const boxRef = useRef();
  const isVisible = useIntersection(boxRef, '30%');
  const router = useRouter();
  const btnRef = useRef();
  const isWebBtnVisible = useIntersection(btnRef);

  useEffect(() => {
    if (isVisible) {
      // setElementInView(item.classStyle);
      setElementInView((prev) => {
        if (prev.slice(-1) - item.classStyle.slice(-1) > 1) {
          return item.classStyle.slice(0, -1) + (prev.slice(-1) - 1);
        }
        if (index === 1 && isVisible && !isWebBtnVisible) {
          return item.classStyle.slice(0, -1) + (prev.slice(-1) - 1) ===
            'slide1'
            ? 'slide1'
            : 'slide2';
        }
        return item.classStyle;
      });
    }
  }, [isVisible, isWebBtnVisible]);

  useEffect(() => {
    if (isWebBtnVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add('webrotateMe');
      }, 3000);
    } else {
      btnRef?.current?.classList.remove('webrotateMe');
    }
    return () => btnRef?.current?.classList.remove('webrotateMe');
  }, [isWebBtnVisible]);

  return (
    <section
      ref={boxRef}
      key={index}
      className={`${item.classStyle} slides slide-sec ${
        lastSlide ? '' : 'slide-sec-carousel'
      }`}
      id={item.classStyle}
    >
      <div className="slider-body">
        <figure className="img-box">
          <img
            loading="lazy"
            src={item.imgUrl}
            alt=""
            className="carousel-img"
          />
          <div className="tag"> {item.imgTitle} </div>
        </figure>
        <div className="carousel-text-div">
          <div className="heading-sec">
            <h1 className="carousel-heading title1">{item.title}</h1>
          </div>
          <div className="carousel-subtext">
            <p className="description">{item.description}</p>
          </div>
        </div>
      </div>
      <div className="bg-style" style={{ background: item.bgColor }}>
        <button
          onClick={() => router.push(`/section/${item?.title.toLowerCase()}`)}
          className={'discover-btn'}
          ref={btnRef}
        >
          <img
            src="/images/discover-web.svg"
            alt="discover-logo"
            className={'img-style'}
          />
          <img
            src="/images/discoverArrow.svg"
            alt="discover-logo"
            className={'img-style-arrow'}
          />
        </button>
      </div>
    </section>
  );
};
