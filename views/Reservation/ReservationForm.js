import React, { useContext, useState } from 'react';
import Checkbox from '../../components/Checkbox/Checkbox';
import styles from './Reservation.module.scss';
import { MILLGROVE_GRADIENT_LEAF } from '../../utils/assets';
import { AuthContext } from '../../context/AuthContext';
import axios from 'axios';
import { apiKey, baseUrl } from '../../utils/constants';

const Input = ({ placeholder, type = 'text', value }) => {
  return (
    <input
      disabled
      type={type}
      className={styles.formInput}
      placeholder={placeholder}
      value={value}
    />
  );
};

const ReservationForm = () => {
  const {
    userToken,
    setUserDetails,
    userDetails: { name, email, phone },
  } = useContext(AuthContext);
  const [isBoxChecked, setIsBoxChecked] = useState(true);
  const [errorMsg, setErrorMsg] = useState('');

  const requestVisit = async () => {
    if (!isBoxChecked) {
      setErrorMsg('Please select the checkbox');
      return;
    }
    try {
      const res = await axios.post(
        `${baseUrl}/client/request-visit`,
        {
          request_date: new Date().toISOString(),
        },
        {
          headers: {
            'rest-api-key': apiKey,
            Authorization: `Bearer ${userToken}`,
          },
        }
      );
      if (res?.status === 200) {
        localStorage.setItem(
          'userToken',
          JSON.stringify({
            authToken: userToken,
            name: name,
            email: email,
            phone: phone,
            visitRequested: true,
          })
        );
        setUserDetails((prev) => ({ ...prev, visitRequested: true }));
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={styles.formWrapper}>
      <div className={styles.formBg}>
        <div className={styles.zIndexStyle}>
          <div className={styles.formHeadingWrapper}>
            <h1 className={styles.formHeading}>
              Request <span>a</span> Visit
              <MILLGROVE_GRADIENT_LEAF className={styles.gradientLeaf} />
            </h1>
          </div>
          <form className={styles.formContainer}>
            <div className={styles.inputSection}>
              <div className={styles.formInputWrapper}>
                <Input value={name || ''} placeholder={'Name'} type="text" />
              </div>
              <div className={styles.formInputWrapper}>
                <Input value={phone || ''} placeholder={'Phone'} type="text" />
              </div>
              <div className={styles.formInputWrapper}>
                <Input value={email || ''} placeholder={'Email'} type="email" />
              </div>
            </div>
            <div className={styles.agreementCheck}>
              <Checkbox
                isBoxChecked={isBoxChecked}
                setIsBoxChecked={setIsBoxChecked}
              />
              <p>I accept the terms for processing my personal data</p>
            </div>
          </form>
          {errorMsg && (
            <div style={{ fontSize: '1rem', color: 'red' }}>{errorMsg}</div>
          )}
          <button onClick={requestVisit} className={styles.requestAVisitBtn}>
            request visit
          </button>
        </div>
      </div>
    </div>
  );
};

export default ReservationForm;
