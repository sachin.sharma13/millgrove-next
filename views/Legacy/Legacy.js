import React, { useEffect, useRef } from 'react';
import Header from '../../components/Header/Header';
import Footer from '../Footer';
import styles from './Legacy.module.scss';
import Timeline from './Timeline';
import { timelineData } from '../../utils/timelineData';
import { useRouter } from 'next/router';
import { cloudFrontUrl } from '../../utils/constants';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { animationGenrator } from '../../utils/assets';
import { useIntersection } from '../../hooks/useIntersection';
import PrivateRoute from '../../components/PrivateRoute';
import { useIsDesktop } from '../../hooks/useIsDesktop';
import { useIsMobile } from '../../hooks/useIsMobile';

const Legacy = () => {
  const router = useRouter();
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const timerRef = useRef();
  const [isMobile] = useIsMobile();
  const [isDesktop] = useIsDesktop();

  useEffect(() => {
    animationGenrator('.first-text', '#firstSection');
    setTimeout(() => {
      animationGenrator('.second-text', '#secondSection');
      // animationGenrator(".third-text", "#thirdSection");

      for (let i = 0; i < timelineData.length; i++) {
        animationGenrator(`.third-text${i}`, `#thirdSection${i}`);
      }
    }, 1000);
  }, []);

  useEffect(() => {
    if (isVisible && btnRef) {
      timerRef.current = setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => {
      btnRef?.current?.classList.remove(styles.rotateMe);
      clearTimeout(timerRef?.current);
    };
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);

  return (
    <PrivateRoute>
      <AnimatedLayout>
        <div className={`${styles['legacy-page']} legacy-page animation-block`}>
          <Header isFooterVisible={isMobileBtnVisible || isVisible} />
          <section
            id="firstSection"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/legacy-banner-latest.webp)`,
              filter: 'saturate(1.22)',
            }}
            className={styles['main-image']}
          >
            <div className={styles['dark-backdrop']}></div>
            <div className={`${styles['main-title']} first-text`}>Legacy</div>
            <div className="tag bottom green"> Lifestyle Image </div>
          </section>
          <section id="secondSection" className={styles['second-section']}>
            <div className={styles['description']}>
              <div className={styles['title']}>
                <h2 className={`${styles['first-row']} second-text`}>
                  Rooted<span>in</span>Heritage
                </h2>
              </div>
              <div className={`${styles['content']} second-text`}>
                The origins of Millgrove date back over 30 years. For many
                years, the land was home to a state-of-the-art rice mill
                financed by the World Bank Group. After the sale of that
                business, the founding family wished to leave a legacy to be
                proud of. Millgrove is the manifestation of that ambition and
                the family have retained several plots for their personal homes.
              </div>
            </div>
          </section>
          <section className={styles['timeline']}>
            <Timeline timelineData={timelineData} />
          </section>
          <section className={styles['footer']}>
            <div className={styles.theHomeSection}>
              <div className={styles.homeSectionStyle}>
                <div className={styles.groupSec}>
                  <div className={styles.headSec}>
                    <span className={styles.textNotera}>The</span>
                    <div className={`${styles.titleCustom}`}>Location</div>
                  </div>

                  {/* Hide on Desktop */}
                  <button
                    onClick={() => router.push('/section/location')}
                    className={styles.discoverBtnMobile}
                    ref={mobileBtnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                </div>

                <div className={styles.assetsSec}>
                  <button
                    onClick={() => router.push('/section/location')}
                    className={styles.discoverBtn}
                    ref={btnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                  <div
                    className={styles.imgBoxStyle}
                    data-aos="fade-up"
                    data-aos-duration="1000"
                  >
                    <img
                      src={`${cloudFrontUrl}/location-banner.webp`}
                      alt="millgrove properties site"
                      className={styles.imgStyle}
                    />
                    <div
                      className={`tag md up ${
                        isMobile || isDesktop ? '' : 'darkGreen'
                      }`}
                    >
                      Actual Photo
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </section>
        </div>
      </AnimatedLayout>
    </PrivateRoute>
  );
};

export default Legacy;
