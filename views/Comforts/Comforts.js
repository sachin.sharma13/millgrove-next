import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import Header from '../../components/Header/Header';
import { cloudFrontUrl } from '../../utils/constants';
import { animationGenrator, newComfortsSlides } from '../../utils/assets';
import Footer from '../Footer';
import styles from './Comforts.module.scss';
import ComfortCarousel from './ComfortsCarousel';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { useIntersection } from '../../hooks/useIntersection';
import PrivateRoute from '../../components/PrivateRoute';
import ReactSimplyCarousel from 'react-simply-carousel';
import { useIsDesktop } from '../../hooks/useIsDesktop';
import { useIsMobile } from '../../hooks/useIsMobile';

const Comforts = () => {
  const router = useRouter();
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const carouselRef = useRef();
  const isFirstCarouselVisible = useIntersection(carouselRef);
  const [currentSlideFirst, setCurrentSlideFirst] = useState(0);
  const [isMobile] = useIsMobile();
  const [isDesktop] = useIsDesktop();

  useEffect(() => {
    animationGenrator('.first-text', '#firstSection');
    setTimeout(() => {
      animationGenrator('.second-text', '#secondSection');
      animationGenrator('.third-text', '#thirdSection');
    }, 1000);
  }, []);

  useEffect(() => {
    if (isVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => btnRef?.current?.classList.remove(styles.rotateMe);
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);

  return (
    <PrivateRoute>
      <AnimatedLayout>
        <div className={`${styles['comforts-page']} animation-block`}>
          <Header isFooterVisible={isMobileBtnVisible || isVisible} />
          <section
            id="firstSection"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/couple-walking-small.webp)`,
              filter: 'saturate(1.22)',
            }}
            className={styles['main-image']}
          >
            <div className={styles['dark-backdrop']}></div>
            <div className={`${styles['main-title']} first-text`}>Comforts</div>
            <div className="tag bottom green"> Lifestyle Image </div>
          </section>
          <section id="secondSection" className={styles['second-section']}>
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="second-text">
                  <span>our</span>Hassle-free
                </div>
                <div className="second-text">Philosophy</div>
              </div>
              <div className={`${styles['content']} second-text`}>
                Millgrove is designed to be different and much consideration has
                gone into finding ways to enrich the quality of life for its
                residents. The delightful built-in comforts go a long way to
                lighten the load of living on your own land.
              </div>
            </div>
          </section>
          <section
            className={`${styles['comforts-third-section']} animation-block`}
            id="thirdSection"
          >
            <div className={`${styles['images-div']}`}>
              <div className={`${styles['image-container']} third-text`}>
                <img
                  src={`${cloudFrontUrl}/woman-arms-out-small.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={`${styles['image-container']} third-text`}>
                <img
                  src={`${cloudFrontUrl}/oldies-juice-small.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={`${styles['image-container']} third-text`}>
                <img
                  src={`${cloudFrontUrl}/parents-walking-child-small.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                  className={styles['positioned-img']}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} second-text`}
              ref={carouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideFirst}
                onRequestChange={setCurrentSlideFirst}
                speed={400}
                infinite={true}
                autoplay={isFirstCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/woman-arms-out-mobile.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/oldies-juice-small.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/parents-walking-child-small.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <section style={{ backgroundColor: '#f9f6ed' }}>
            <ComfortCarousel
              primaryColor={'#f9f6ed'}
              secondaryColor={'#6b4637'}
              slidesList={newComfortsSlides}
            />
          </section>
          <section className={styles['footer']}>
            <div className={styles.theHomeSection}>
              <div className={styles.homeSectionStyle}>
                <div className={styles.groupSec}>
                  <div className={styles.headSec}>
                    <span className={styles.textNotera}>The</span>
                    <div className={`${styles.titleCustom}`}>Legacy</div>
                  </div>

                  {/* Hide on Desktop */}
                  <button
                    onClick={() => router.push('/section/legacy')}
                    className={styles.discoverBtnMobile}
                    ref={mobileBtnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                </div>

                <div className={styles.assetsSec}>
                  <button
                    onClick={() => router.push('/section/legacy')}
                    className={styles.discoverBtn}
                    ref={btnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                  <div
                    className={styles.imgBoxStyle}
                    data-aos="fade-up"
                    data-aos-duration="1000"
                  >
                    <img
                      src={`${cloudFrontUrl}/legacy-banner-latest.webp`}
                      alt="millgrove properties site"
                      className={styles.imgStyle}
                    />
                    <div
                      className={`tag md up ${
                        isMobile || isDesktop ? '' : 'darkGreen'
                      }`}
                    >
                      Lifestyle Image
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </section>
        </div>
      </AnimatedLayout>
    </PrivateRoute>
  );
};

export default Comforts;
