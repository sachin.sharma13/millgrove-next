/* eslint-disable @next/next/no-img-element */
import { useContext, useEffect } from 'react';
import { AuthContext } from '../../context/AuthContext';
import { cloudFrontUrl } from '../../utils/constants';
import styles from './Reservation.module.scss';
import ReservationForm from './ReservationForm';
import TreePrint from './TreePrint';
import RequestSubmittedCard from './RequestSubmittedCard';
import { useRouter } from 'next/router';
import { useIsMobile } from '../../hooks/useIsMobile';

const Reservation = ({
  mobileBtnRef,
  isMobileBtnVisible,
  btnRef,
  isVisible,
}) => {
  const {
    userDetails: { visitRequested },
  } = useContext(AuthContext);
  const router = useRouter();
  const [isMobile] = useIsMobile();

  useEffect(() => {
    if (isVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => btnRef?.current?.classList.remove(styles.rotateMe);
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);
  return (
    <div className={styles.wrapper}>
      <div className={styles.bgSection}>
        <div
          className={styles.reservationCard}
          data-aos="fade-up"
          data-aos-duration="1000"
        >
          <div className={styles.treeSection}>
            <TreePrint className={styles.treePrint} />
          </div>
          {visitRequested ? <RequestSubmittedCard /> : <ReservationForm />}
        </div>
      </div>

      <div className={styles.theHomeSection}>
        <div className={styles.homeSectionStyle}>
          <div className={styles.groupSec}>
            <div className={styles.headSec}>
              <span className={styles.textNotera}>The</span>
              <div className={`${styles.titleCustom}`}>Location</div>
            </div>

            {/* Hide on Desktop */}
            <button
              onClick={() => router.push('/section/location')}
              className={styles.discoverBtnMobile}
              ref={mobileBtnRef}
            >
              <img
                src="/images/discover-web.svg"
                alt="discover-logo"
                className={styles.imgStyle}
              />
              <img
                src="/images/discoverArrow.svg"
                alt="discover-logo"
                className={styles.imgStyleArrow}
              />
            </button>
          </div>

          <div className={styles.assetsSec}>
            <button
              onClick={() => router.push('/section/location')}
              className={styles.discoverBtn}
              ref={btnRef}
            >
              <img
                src="/images/discover-web.svg"
                alt="discover-logo"
                className={styles.imgStyle}
              />
              <img
                src="/images/discoverArrow.svg"
                alt="discover-logo"
                className={styles.imgStyleArrow}
              />
            </button>
            <div
              className={styles.imgBoxStyle}
              data-aos="fade-up"
              data-aos-duration="1000"
            >
              <img
                src={`${cloudFrontUrl}/location-banner.webp`}
                alt="millgrove properties site"
                className={styles.imgStyle}
              />
              <div className={`tag md ${isMobile ? '' : 'darkGreen'}`}> Actual Photo </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Reservation;
