import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import Comforts from '../../../views/Comforts/Comforts';
import Grounds from '../../../views/Grounds/Grounds';

import Homes from '../../../views/Homes/Homes';
import Location from '../../../views/Location/Location';
import Legacy from '../../../views/Legacy/Legacy';

function Section(props) {
  const router = useRouter();
  const id = router.query.id;

  const getSection = (id) => {
    switch (id) {
      case 'homes':
        return <Homes />;
      case 'grounds':
        return <Grounds />;
      case 'comforts':
        return <Comforts />;
      case 'location':
        return <Location />;
      case 'legacy':
        return <Legacy />;
      default:
        return <div></div>;
    }
  };

  return <div>{getSection(id)}</div>;
}

export default Section;
