import React from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import Button from '../../components/Button';
import { apiKey, baseUrl } from '../../utils/constants';
import styles from './unsubscribe.module.scss';
import { toast } from 'react-toastify';
const Unsubscribe = () => {
  const router = useRouter();
  const id = router.query.id;

  const unsubscribeUser = async () => {
    try {
      const res = await axios.post(
        `${baseUrl}/fsm/unsubscribe-email`,
        {
          id,
        },
        {
          headers: {
            'rest-api-key': apiKey,
          },
        }
      );
      if (res?.status === 200) {
        toast('Unsubscribed successfully!');
        setTimeout(() => {
          router.push('/');
        }, 1500);
      }
    } catch (err) {
      console.log(err);
      toast('Please try again!');
    }
  };
  return (
    <div className={styles.wrapper}>
      <div>
        <h3>
          Are you sure you wish to unsubscribe from our email notifications?
        </h3>
        <Button
          text={'Unsubscribe'}
          variant="primary"
          classname={'button-style48'}
          clickhandler={unsubscribeUser}
        />
      </div>
    </div>
  );
};

export default Unsubscribe;
