import React, { useEffect, useRef, useState } from 'react';
import Button from '../../components/Button';
import Checkbox from '../../components/Checkbox/Checkbox';
import Layout from '../../components/Layout';
import PhoneInput from 'react-phone-number-input';
import { MILLGROVE_TREE } from '../../utils/assets';
import styles from './Signup.module.scss';
import axios from 'axios';
import { apiKey, baseUrl } from '../../utils/constants';
import Header from '../../components/Header';
import { WarningOctagon } from '../../public/icons/icons';
import CountrySelect from '../../components/CountrySelect';
import { checkNonNumericInput } from '../../utils/checkNonNumericInput';
import { isPhoneNosValid } from '../../utils/isPhoneNosValid';
import { toast } from 'react-toastify';

const ERROR_MSG = 'This is a required field';
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const Signup = ({
  isRegistering,
  setIsRegistering,
  setIsRegisterationSuccessfull,
  setIsLoggingIn,
}) => {
  const [userInfo, setUserInfo] = useState({ firstName: '', lastName: '', phone: '', email: '' });
  const [isBoxChecked, setIsBoxChecked] = useState(false);
  const [error, setError] = useState({
    errorOccured: false,
    firstNameError: '',
    lastNameError: '',
    phoneError: '',
    emailError: '',
  });
  const [selectedCountry, setSelectedCountry] = useState({
    name: 'India',
    dial_code: '91',
    code: 'IN',
  });
  const [apiError, setApiError] = useState('');
  const firstNameRef = useRef();
  const lastNameRef = useRef();

  const isAnyFieldEmpty = () => {
    if (!userInfo.firstName.trim()) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        firstNameError: ERROR_MSG,
      }));
      return true;
    }
    if (!userInfo.lastName.trim()) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        lastNameError: ERROR_MSG,
      }));
      return true;
    }
    if (!userInfo.phone) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        phoneError: ERROR_MSG,
      }));
      return true;
    }
    if (!userInfo.email) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        emailError: ERROR_MSG,
      }));
      return true;
    }
    if (!emailRegex.test(userInfo.email)) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        emailError: 'Invalid email address',
      }));
      return true;
    }
    if (!isPhoneNosValid(userInfo.phone, selectedCountry.dial_code)) {
      setError((prev) => ({
        ...prev,
        errorOccured: true,
        phoneError: 'Invalid phone number',
      }));
      return true;
    }
    if (!isBoxChecked) return true;
    return false;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (isAnyFieldEmpty()) return;

    setError((prev) => ({
      ...prev,
      errorOccured: false,
      firstNameError: '',
      lastNameError: '',
      phoneError: '',
      emailError: '',
    }));

    try {
      const res = await axios.post(
        `${baseUrl}/client/register`,
        {
          name: userInfo.firstName + ' ' + userInfo.lastName,
          email: userInfo.email,
          phone: `+${selectedCountry.dial_code}${userInfo.phone}`,
        },
        {
          headers: {
            'rest-api-key': apiKey,
          },
        }
      );
      if (res?.status === 200) {
        setIsRegistering(false);
        setIsRegisterationSuccessfull(true);
      }
    } catch (err) {
      console.log(err);
      setApiError(
        err?.response?.data?.message || 'Something is wrong! Please try again'
      );
    }
  };

  const updateUserInfo = (field, e) => {
    setApiError('');
    setError((prev) => ({
      ...prev,
      [`${field}Error`]: '',
    }));
    if (e.target.value === '') {
      setError((prev) => ({
        ...prev,
        [`${field}Error`]: ERROR_MSG,
      }));
    }
    setUserInfo({ ...userInfo, [field]: e.target.value });
  };

  const shouldBtnBeDisabled = () => {
    const data = Object.values(userInfo);
    if (!isBoxChecked) return true;
    if (data.includes('')) return true;
    return false;
  };

  const getEmailErrorMessage = () => {
    if (error.errorOccured && !userInfo.email) {
      return ERROR_MSG;
    }
    if (
      error.errorOccured &&
      !emailRegex.test(userInfo.email) &&
      error.emailError
    ) {
      return 'Invalid email address';
    }
    return '';
  };

  useEffect(() => {
    firstNameRef.current.focus();
  }, []);

  return (
    <Layout layoutStyle="login-page-style">
      <div className={`${styles.signupMainContainer} login-mobile`}>
        <Header />
        {isRegistering && (
          <div className={styles.mainWrapper}>
            <div className={styles.formWrapper}>
              <div
                className={styles.cross}
                onClick={() => setIsRegistering(false)}
              >
                <img src="/images/cross.svg" alt="cross" />
              </div>
              <div className={styles.headingWrapper}>
                <h3 className={styles.heading}>
                  Register <span className={styles.smallText}>your</span>{' '}
                  Details
                </h3>
                <div className={styles.bgTree}>
                  <MILLGROVE_TREE fillColor="#b1bbae" />
                </div>
              </div>
              <p className={styles.fieldRequiredText}>
                (All Fields are required)
              </p>
              <form onSubmit={(e) => handleSubmit(e)}>
                <div className={styles.formInputSection}>
                  <div className={styles.formInputWrapper}>
                    <input
                      ref={firstNameRef}
                      name="firstName"
                      onChange={(e) => updateUserInfo('firstName', e)}
                      type={'text'}
                      className={styles.formInput}
                      placeholder={'First Name'}
                    />
                    {error.errorOccured && !userInfo.firstName.trim() ? (
                      <span className={styles.warningIconWrapper}>
                        <WarningOctagon />
                      </span>
                    ) : null}
                    <p
                      style={{
                        opacity: error.errorOccured && !userInfo.firstName.trim() ? 1 : 0,
                      }}
                      className={styles.errorText}
                    >
                      {ERROR_MSG}
                    </p>
                  </div>
                  <div className={styles.formInputWrapper}>
                    <input
                      ref={lastNameRef}
                      name="lastName"
                      onChange={(e) => updateUserInfo('lastName', e)}
                      type={'text'}
                      className={styles.formInput}
                      placeholder={'Last Name'}
                    />
                    {error.errorOccured && !userInfo.lastName.trim() ? (
                      <span className={styles.warningIconWrapper}>
                        <WarningOctagon />
                      </span>
                    ) : null}
                    <p
                      style={{
                        opacity: error.errorOccured && !userInfo.lastName.trim() ? 1 : 0,
                      }}
                      className={styles.errorText}
                    >
                      {ERROR_MSG}
                    </p>
                  </div>

                  <div className={styles.phoneNosWrapper}>
                    <CountrySelect
                      selectedCountry={selectedCountry}
                      setSelectedCountry={setSelectedCountry}
                    />
                    <input
                      className={styles.phoneNumberInput}
                      type={'text'}
                      placeholder="Phone Number"
                      value={userInfo.phone}
                      onChange={(e) => updateUserInfo('phone', e)}
                      onKeyDown={(e) => checkNonNumericInput(e)}
                      inputMode="numeric"
                    />
                    {(error.errorOccured && !userInfo.phone) ||
                    (!isPhoneNosValid(
                      userInfo.phone,
                      selectedCountry.dial_code
                    ) &&
                      error.phoneError) ? (
                      <span className={styles.warningIconWrapper}>
                        <WarningOctagon />
                      </span>
                    ) : null}
                    <p
                      style={{
                        opacity:
                          (error.errorOccured && !userInfo.phone) ||
                          !isPhoneNosValid(
                            userInfo.phone,
                            selectedCountry.dial_code
                          )
                            ? 1
                            : 0,
                        marginLeft: 0,
                      }}
                      className={styles.errorText}
                    >
                      {error.phoneError}
                    </p>
                  </div>

                  <div className={styles.formInputWrapper}>
                    <input
                      name="email"
                      onChange={(e) => updateUserInfo('email', e)}
                      type={'text'}
                      className={styles.formInput}
                      placeholder={'Email'}
                    />
                    {(error.errorOccured &&
                      !userInfo.email &&
                      error.emailError) ||
                    (error.errorOccured &&
                      !emailRegex.test(userInfo.email) &&
                      error.emailError) ? (
                      <span className={styles.warningIconWrapper}>
                        <WarningOctagon />
                      </span>
                    ) : null}
                    <p
                      style={{
                        opacity:
                          (error.errorOccured && !userInfo.email) ||
                          (error.errorOccured &&
                            !emailRegex.test(userInfo.email))
                            ? 1
                            : 0,
                      }}
                      className={styles.errorText}
                    >
                      {getEmailErrorMessage()}
                    </p>
                  </div>
                </div>
                <p
                  style={{
                    opacity: apiError !== '' ? 1 : 0,
                    color: '#9f5454',
                    fontSize: '0.875rem',
                  }}
                >
                  {apiError.includes('logging') ? (
                    <>
                      Phone & email already registered! Please try{' '}
                      <span
                        onClick={() => {
                          setIsRegistering(false);
                          setIsLoggingIn(true);
                        }}
                        style={{
                          cursor: 'pointer',
                          textDecoration: 'underline',
                        }}
                      >
                        logging
                      </span>{' '}
                      in
                    </>
                  ) : (
                    apiError
                  )}
                </p>
                <p className={styles.accountExistsText}>
                  Already registered? Please{' '}
                  <span
                    onClick={() => {
                      setIsRegistering(false);
                      setIsLoggingIn(true);
                    }}
                  >
                    login
                  </span>
                </p>
                <div className={styles.agreementCheck}>
                  <Checkbox
                    isBoxChecked={isBoxChecked}
                    setIsBoxChecked={setIsBoxChecked}
                  />
                  <p>I accept the terms for processing my personal data</p>
                </div>

                <div className={styles.submitBtnWrapper}>
                  <Button
                    isDisabled={shouldBtnBeDisabled()}
                    text={'Continue'}
                    type="submit"
                    classname="button-style48"
                  />
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default Signup;
