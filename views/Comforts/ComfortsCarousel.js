/* eslint-disable @next/next/no-img-element */
import { useEffect } from 'react';
import { animationGenrator } from '../../utils/assets';
import { cloudFrontUrl } from '../../utils/constants';
import styles from './Comforts.module.scss';
import gsap from 'gsap';
import { useIsMobile } from '../../hooks/useIsMobile';

export const CarouselSlide = ({
  imageUrl,
  content1,
  content2,
  primaryColor,
  secondaryColor,
  idx,
  isLocation,
  isLast,
  imgSource,
  imgTitle,
}) => {
  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
    animationGenrator(`.comfort-text${idx}`, `#comfortSection${idx}`);
  }, []);
  const isMobile = useIsMobile();

  return (
    <>
      {isLocation && idx === 0 ? (
        <></>
      ) : (
        <section
          className={`${styles['carousel-slide']} animation-block`}
          style={{
            backgroundColor: idx % 2 !== 0 ? primaryColor : secondaryColor,
            paddingBottom: isLast ? '180px' : undefined,
          }}
          id={`comfortSection${idx}`}
        >
          <div className={`${styles['image-container']} comfort-text${idx}`}>
            <img
              loading="lazy"
              src={imageUrl || `${cloudFrontUrl}/2021.webp`}
            />
            {imgSource}
            <div className='tag' style={{backgroundColor: idx % 2 !== 0 ? primaryColor : secondaryColor, color: idx % 2 === 0 ? 'white' : 'black'}}> {imgTitle} </div>
          </div>
          <div
            style={{
              color: idx % 2 === 0 ? '#ebe5d3' : '#262e2f',
              alignSelf: content2 ? 'center' : 'start',
              marginTop: content2 && !isMobile ? 'unset' : '36px',
            }}
            className={styles['content-container']}
          >
            <div className={styles['content']}>
              <h3 className={`comfort-text${idx}`}>{content1?.heading}</h3>
              <p className={`comfort-text${idx}`}>{content1?.description}</p>
            </div>
            <div
              style={{
                backgroundColor: idx % 2 === 0 ? primaryColor : secondaryColor,
              }}
              className={styles['separator-line']}
            ></div>
            {content2 && (
              <div className={styles['content']}>
                <h3 className={`comfort-text${idx}`}>{content2?.heading}</h3>
                <p className={`comfort-text${idx}`}>{content2?.description}</p>
              </div>
            )}
          </div>
        </section>
      )}
    </>
  );
};

const ComfortCarousel = ({
  slidesList,
  primaryColor,
  secondaryColor,
  isLocation,
}) => {
  // useEffect(() => {
  //   let controller = new ScrollMagic.Controller({
  //     globalSceneOptions: {
  //       triggerHook: 0,
  //       duration: 1300,
  //       offset: 0,
  //     },
  //   });

  //   let slides = document.querySelectorAll("section.slide-sec");
  //   for (let i = 0; i < slides.length; i++) {
  //     let animation = TweenMax.to(slides[i], 0.7, {
  //       scale: 0,
  //       opacity: 0,
  //       ease: Quad.easeInOut,
  //     });

  //     new ScrollMagic.Scene({
  //       triggerElement: slides[i],
  //     })
  //       .setPin(slides[i], { pushFollowers: false })
  //       .addTo(controller)
  //       .setTween(animation);
  //   }
  // }, []);

  return (
    <div className="millglove-slider-section-comforts revamped-carousel-container">
      {slidesList.map((slide, idx) => (
        <div key={idx}>
          <CarouselSlide
            key={slide.imgUrl}
            imageUrl={slide.imgUrl}
            content1={slide.content1}
            content2={slide?.content2}
            primaryColor={primaryColor}
            secondaryColor={secondaryColor}
            idx={idx}
            isLocation={isLocation}
            isLast={idx === slidesList.length - 1}
            imgSource={slide.imgSource ?? <></>}
            imgTitle={slide.imgTitle}
          />
        </div>
      ))}
    </div>
  );
};

export default ComfortCarousel;
