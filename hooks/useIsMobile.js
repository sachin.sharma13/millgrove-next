import { useEffect, useState } from 'react';

const useIsMobile = () => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.matchMedia('(max-width: 700px)').matches);
    };

    if (typeof window !== null) {
      setIsMobile(window.matchMedia('(max-width: 700px)').matches);
    }

    // window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return [isMobile];
};

export { useIsMobile };
