/* eslint-disable @next/next/no-img-element */
import styles from './Footer.module.scss';
import { MillgroveLogo } from '../../utils/millgrove';
import { useRouter } from 'next/router';

const Footer = () => {
  const router = useRouter();
  return (
    <div className={styles.footerHomepage}>
      <div className={styles.footerLine}></div>
      <div className={styles.footerWrapper}>
        <div className={styles.themeText}>
          <div className={styles.themeText2}>
            <MillgroveLogo className="svgFill" />
          </div>
        </div>
        <div className={styles.footerLinksWrapper}>
          <div className={styles.footerLinks}>
            <a
              onClick={() => router.push('/privacy-policy')}
              className="hover-underline-animation"
            >
              Privacy Policy
            </a>
            <a
              onClick={() => router.push('/terms-and-conditions')}
              className="hover-underline-animation"
            >
              Terms & Conditions
            </a>
          </div>
        </div>
        <div className={styles['reraDetails']}>
          <p>RERA REGISTRATION NO.: RC/REP/HARERA/GGM/696/428/2023/40</p>
          <a
            className="siteUrl hover-underline-animation"
            href={'https://haryanarera.gov.in/'}
            target="#blank"
          >
            www.haryanarera.gov.in
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
