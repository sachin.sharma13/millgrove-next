const baseUrl = process.env.NEXT_PUBLIC_API_BASE_URL;
const apiKey = process.env.NEXT_PUBLIC_REST_API_KEY;
const cloudFrontUrl = process.env.NEXT_PUBLIC_CLOUDFRONT_URL;
const mapboxToken = process.env.NEXT_PUBLIC_MAPBOX_TOKEN;

export { baseUrl, apiKey, cloudFrontUrl, mapboxToken };
