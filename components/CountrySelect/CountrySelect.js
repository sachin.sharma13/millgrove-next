import React, { useRef, useState } from 'react';
import styles from './CountrySelect.module.scss';
import { DownArrow } from '../../public/icons/icons';
import { allCountries, searchCountries } from '../../utils/countries';
import { useClickOutside } from '../../hooks/useClickOutside';
import { useEffect } from 'react';

const CountrySelect = ({ selectedCountry, setSelectedCountry, numberRef }) => {
  const [myCountries, setMyCountries] = useState(allCountries);
  const [showDropdown, setShowDropdown] = useState(false);
  const [searchCountry, setSearchCountry] = useState('');
  const inputRef = useRef();
  const clickRef = useRef();
  const onClickOutside = () => setShowDropdown(false);
  useClickOutside(clickRef, onClickOutside);
  const searchRef = useRef();

  useEffect(() => showDropdown && setTimeout(() => {
    inputRef.current.focus()
  }, 300), [showDropdown]);

  const countryClickHandler = () => {
    !showDropdown && setSearchCountry(''), setMyCountries(allCountries);
    setShowDropdown((prev) => !prev);
  };

  return (
    <>
      <div ref={clickRef} className={styles.countrySelectWrapper}>
        <label>
          {selectedCountry && (
            <img
              className={styles.selectedCountryFlag}
              alt={selectedCountry.name}
              height="20"
              width="20"
              src={`http://purecatamphetamine.github.io/country-flag-icons/1x1/${selectedCountry.code}.svg`}
              onClick={() => countryClickHandler()}
            />
          )}
        </label>
        <span className={styles.dialCode} onClick={() => countryClickHandler()}>
          +{selectedCountry.dial_code}
        </span>
        <span
          className={`${styles.downArrow} ${showDropdown && styles.up}`}
          onClick={() => countryClickHandler()}
        >
          <DownArrow />
        </span>
        {showDropdown && (
          <ul className={styles.listWrapper}>
            <input
              ref={inputRef}
              placeholder="Enter Country Name"
              type={'text'}
              value={searchCountry}
              onClick={() => {
                setSearchCountry('');
                setMyCountries(allCountries);
              }}
              onChange={(e) => {
                setSearchCountry(e.target.value);
                searchCountries(e.target.value, searchRef, setMyCountries);
              }}
              className={styles.flagInput}
            />
            {myCountries.length ? (
              myCountries.map((country) => (
                <li
                  key={country.code}
                  className={styles.listChild}
                  onClick={() => {
                    setSearchCountry(`${country.name} ${country.dial_code}`);
                    setSelectedCountry({
                      name: country.name,
                      dial_code: country.dial_code,
                      code: country.code,
                    });
                    setShowDropdown(false);
                    numberRef?.current?.focus();
                  }}
                >
                  <img
                    alt={country.name}
                    height="16"
                    width="16"
                    src={`http://purecatamphetamine.github.io/country-flag-icons/1x1/${country.code}.svg`}
                    className={styles.flagImg}
                  />
                  <span>
                    {country.name} +{country.dial_code}
                  </span>
                </li>
              ))
            ) : (
              <div className={styles.nodata}>No match found</div>
            )}
          </ul>
        )}
      </div>
    </>
  );
};

export default CountrySelect;
