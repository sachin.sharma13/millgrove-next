import { useEffect, useState } from 'react';
import { LEFT_ARROW, RIGHT_ARROW } from '../utils/assets';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

export const AccordionCarousel = ({
  title,
  features,
  styles,
  index,
  currentSlide,
  setCurrentSlide,
}) => {
  const featureKeys = Object.keys(features);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [imageLink, setImageLink] = useState(
    features[featureKeys[currentImageIndex]]
  );
  const [subIndex, setSubIndex] = useState(0);

  useEffect(() => {
    setImageLink(features[featureKeys[currentImageIndex]]);
  }, [currentImageIndex]);

  const changeImage = (newImageIndex) => {
    setCurrentImageIndex(newImageIndex);
  };

  return (
    <div
      onClick={() => setCurrentSlide(index)}
      className={currentSlide === index ? styles['selected'] : ''}
      style={
        currentSlide === index
          ? {
              backgroundImage: `url(${imageLink[subIndex]})`,
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
            }
          : {}
      }
    >
      <div className={styles['title-name']}>{title}</div>
      {currentSlide === index && (
        <div className={styles['dark-backdrop']}></div>
      )}
      <div className={styles['arrows']}>
        {imageLink.length > 1 && (
          <>
            <div
              onClick={() =>
                setSubIndex(
                  subIndex === imageLink.length - 1 ? 0 : subIndex + 1
                )
              }
              className={styles['arrow-container']}
              style={subIndex === 0 ? { display: 'none' } : undefined}
            >
              <div>Front</div>
              <LEFT_ARROW className={styles['left-arrow']} />
            </div>
            <div
              onClick={() =>
                setSubIndex(
                  subIndex === 0 ? imageLink.length - 1 : subIndex - 1
                )
              }
              style={subIndex !== 0 ? { display: 'none' } : undefined}
              className={styles['arrow-container']}
            >
              <RIGHT_ARROW className={styles['right-arrow']} />
              <div>Rear</div>
            </div>
          </>
        )}
      </div>
      <div className={styles['features']}>
        {featureKeys.map((item, index) => (
          <div
            key={index}
            className={
              currentImageIndex === index ? styles['active'] : undefined
            }
            onClick={() => changeImage(index)}
            style={{
              textTransform: item.includes('ft') ? 'unset' : 'uppercase',
            }}
            dangerouslySetInnerHTML={{ __html: item }}
          ></div>
        ))}
      </div>
    </div>
  );
};
