import mapboxgl from 'mapbox-gl';
import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import CollapseBar from '../../components/CollapseBar';
import Header from '../../components/Header/Header';
import {
  animationGenrator,
  newLocationSlides,
  stores,
} from '../../utils/assets';
import Footer from '../Footer';
import styles from './Location.module.scss';
import { cloudFrontUrl, mapboxToken } from '../../utils/constants';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { useIntersection } from '../../hooks/useIntersection';
import PrivateRoute from '../../components/PrivateRoute';
import ComfortCarousel from '../Comforts/ComfortsCarousel';
import gsap from 'gsap';
import ReactSimplyCarousel from 'react-simply-carousel';
import { useMapConfig } from '../../hooks/useMapConfig';
import { useIsMobile } from '../../hooks/useIsMobile';
import { useIsDesktop } from '../../hooks/useIsDesktop';

stores.features.forEach(function (store, i) {
  store.properties.id = i;
});

const storeTypes = stores.features.reduce((result, obj) => {
  if (Object.keys(result).find((item) => item === obj.type)) {
    return { ...result, [obj.type]: [...result[obj.type], obj] };
  } else {
    return { ...result, [obj.type]: [obj] };
  }
}, {});

const Location = () => {
  const mapContainer = useRef(null);
  const [map, setMap] = useState(null);
  const [currentlyOpen, setCurrentlyOpen] = useState(null);
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const [currentChip, setCurrentChip] = useState(null);
  const firstCarouselRef = useRef();
  const isFirstCarouselVisible = useIntersection(firstCarouselRef);
  const secondCarouselRef = useRef();
  const isSecondCarouselVisible = useIntersection(secondCarouselRef);
  const [currentSlideFirst, setCurrentSlideFirst] = useState(0);
  const [currentSlideSecond, setCurrentSlideSecond] = useState(0);
  const [centerCoordinates, mapZoomLevel] = useMapConfig();
  const [isDesktop] = useIsDesktop();
  const [isMobile] = useIsMobile()

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add('discoverBtnMobile-rotateMe');
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove('discoverBtnMobile-rotateMe');
    }
    return () =>
      mobileBtnRef?.current?.classList.remove('discoverBtnMobile-rotateMe');
  }, [isMobileBtnVisible]);

  useEffect(() => {
    mapboxgl.accessToken = mapboxToken;
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/trlindia/cldoe2e7t000601rufw7p4n9x',
      center: centerCoordinates,
      zoom: mapZoomLevel,
      bearing: 300,
      // maxBounds: bounds,
    });
    setMap(map);
    map.scrollZoom.disable();

    return () => map.remove();
  }, []);

  useEffect(() => {
    animationGenrator('.first-text', '#firstSection');
    setTimeout(() => {
      animationGenrator('.second-text', '#secondSection');
      animationGenrator('.third-text', '#thirdSection');
      animationGenrator('.fourth-text', '#fourthSection');
    }, 1000);
  }, []);

  useEffect(() => {
    if (map) {
      map.on('load', () => {
        const images = [
          '/images/schools-icon.png',
          '/images/shopping-icon.png',
          '/images/hospitals-icon.png',
          '/images/hotels-icon.png',
          '/images/connectivity-icon.png',
          '/images/familyactivities-icon.png',
          '/images/train-icon.png',
        ];

        for (let i = 0; i < images.length; i++) {
          map.loadImage(images[i], function (error, image) {
            if (error) throw error;
            map.addImage(`${images[i].split('-')[0].slice(8)}`, image);
          });
        }

        Object.keys(storeTypes).forEach((item) => {
          const id = item?.split(' ').join('').toLowerCase();
          map.addSource(id, {
            type: 'geojson',
            data: {
              type: 'FeatureCollection',
              features: storeTypes[item],
            },
          });
          map.addLayer({
            id: id,
            type: 'symbol',
            source: id,
            layout: {
              'icon-image': id,
              'text-field': ['get', 'title'],
              'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
              'text-offset': [0, 1.25],
              'text-anchor': 'top',
            },
          });

          const popup = new mapboxgl.Popup({
            closeOnClick: true,
          });

          const showPopUp = (e) => {
            const coordinates = e.features[0].geometry.coordinates.slice();
            const description = e.features[0].properties.name;

            map.getCanvas().style.cursor = 'pointer';

            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
              coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            popup
              .setLngLat(coordinates)
              .setHTML(
                '<h6>' +
                  description +
                  '</h6>' +
                  '\n' +
                  '<h6>' +
                  '<img src="/images/popup-car.png" alt="car" height="20px"/>' +
                  ' ' +
                  e.features[0].properties.driveTime +
                  '</h6>'
              )
              .addTo(map);
          };
          if (window.matchMedia('(min-width: 1025px)').matches) {
            map.on('mouseenter', id, showPopUp);
            map.on('mouseleave', id, () => {
              popup.remove();
              map.getCanvas().style.cursor = '';
            });
          } else {
            map.on('click', id, showPopUp);
          }

          const roadColor = (name) =>
            map.addLayer({
              id: 'road',
              type: 'line',
              source: {
                type: 'vector',
                url: 'mapbox://mapbox.mapbox-streets-v8',
              },
              'source-layer': 'road',
              filter: ['==', 'name', name], // Replace with your road's name
              paint: {
                'line-color': 'black', // Replace with your desired color
              },
            });
        });

        const el = document.createElement('div');
        el.className = styles['millgrove-marker'];
        new mapboxgl.Marker(el)
          .setLngLat([76.97928912837119, 28.39143673546095])
          .addTo(map);
      });
      map.addControl(new mapboxgl.NavigationControl(), 'top-left');
      map.addControl(new mapboxgl.ScaleControl());
    }
  }, [map]);

  useEffect(() => {
    if (isVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => btnRef?.current?.classList.remove(styles.rotateMe);
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);

  const storesHandler = (type) => {
    Object.keys(storeTypes).forEach((item) => {
      if (item === type) {
        map?.getSource(item?.split(' ').join('').toLowerCase()).setData({
          type: 'FeatureCollection',
          features: storeTypes[item],
        });
        return;
      }
      map?.getSource(item?.split(' ').join('').toLowerCase()).setData({
        type: 'FeatureCollection',
        features: [],
      });
    });

    map.flyTo({
      center: centerCoordinates,
      zoom: mapZoomLevel,
      duration: 2000,
      easing: (t) => {
        return t * (2 - t);
      },
    });
  };

  const router = useRouter();
  return (
    <PrivateRoute>
      <AnimatedLayout>
        <div className={`${styles['location-page']} animation-block`}>
          <Header isFooterVisible={isMobileBtnVisible || isVisible} />
          <section
            id="firstSection"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/location-banner.webp)`,
            }}
            className={styles['main-image']}
          >
            <div className={styles['dark-backdrop']}></div>
            <div className={`${styles['main-title']} first-text`}>Location</div>
            <div className="tag bottom green"> Artistic Impression </div>
          </section>
          <section id="secondSection" className={styles['second-section']}>
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="second-text">Perfectly Placed</div>
                <div className="second-text">
                  <span>for a</span> Balanced Life
                </div>
              </div>
              <div className={`${styles['content']} second-text`}>
                Conveniently tucked away in Sector 76, Millgrove is directly
                accessed from NH48 via the 60m 76/77 sector road. The estate’s
                impeccable proximity to several major roads makes the whole of
                the National Capital Region easily reachable, and the vibrant
                local amenities mean everything you need is already on your
                doorstep.
              </div>
            </div>
            <div className={`${styles['images-div']} second-text`}>
              <div className={styles['image-container']}>
                <img
                  className={styles['dad-son-cricket']}
                  src={`${cloudFrontUrl}/dad_son_cricket-small.webp`}
                  // src={`${cloudFrontUrl}/location-image-1.webp`}
                  alt="location-image"
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/old-couple-in-mall.webp`}
                  alt="location-image"
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/friends-at-bar.webp`}
                  alt="location-image"
                  className={styles['third-carousel-image']}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} second-text`}
              ref={firstCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideFirst}
                onRequestChange={setCurrentSlideFirst}
                speed={400}
                infinite={true}
                autoplay={isFirstCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/old-couple-in-mall.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['dad-son-cricket']}
                    src={`${cloudFrontUrl}/dad_son_cricket-small.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/friends-at-bar.webp`}
                    alt="location-image"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/couple-shopping.webp`}
                    loading="lazy"
                    alt="couple-shopping"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <section id="thirdSection" className={styles['third-section']}>
            <div className={styles['text-div']}>
              <div className="third-text">
                Explore <span>your</span>
              </div>
              <div className="third-text">Neighbourhood</div>
            </div>
            <div className={styles['mapbox']}>
              <div className={styles['chips-container']}>
                {Object.keys(storeTypes).map((item, index) => (
                  <div
                    key={index}
                    style={
                      index === currentChip
                        ? {
                            backgroundColor: '#ffffff',
                            border: '3px solid #ff9e41',
                          }
                        : undefined
                    }
                    onClick={() => {
                      setCurrentChip(index);
                      storesHandler(item);
                    }}
                    className={styles['chip']}
                  >
                    {item}
                  </div>
                ))}
              </div>
              <div ref={mapContainer} className={styles['map-container']} />
              <div className={styles['side-bar']}>
                {Object.keys(storeTypes).map((type, index) => (
                  <CollapseBar
                    key={index}
                    styles={styles}
                    type={type}
                    index={index}
                    storeTypes={storeTypes}
                    map={map}
                    currentlyOpen={currentlyOpen}
                    setCurrentlyOpen={setCurrentlyOpen}
                    allFeatures={stores.features}
                  />
                ))}
              </div>
              <p className={styles['tnc']}>
                * estimated drive after Dwarka Expressway & NH48 Cloverleaf
                Flyover complete and Kherki Daula Toll Plaza shifts.
              </p>
            </div>
          </section>
          <section className={styles['school-shopping']}>
            <div className={styles['school-shopping-container']}>
              <div>
                <img src={`${cloudFrontUrl}/kid-at-school.webp`} />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div>
                <img src={`${cloudFrontUrl}/couple-shopping.webp`} />
                <div className="tag md"> Lifestyle Image </div>
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} second-text`}
              ref={secondCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideSecond}
                onRequestChange={setCurrentSlideSecond}
                speed={400}
                infinite={true}
                autoplay={isSecondCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/toy-plane.webp`}
                    loading="lazy"
                    alt="kid-with-toy"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/kid-at-school.webp`}
                    loading="lazy"
                    alt="school-kid"
                  />
                  <div className="tag"> Lifestyle Image </div>
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <section id="fourthSection" className={styles['fourth-section']}>
            <div className={styles['image-and-text-container']}>
              <div className={styles['text-div']}>
                <div className={styles['title']}>
                  <div>
                    <span className="fourth-text">The future is</span>
                  </div>
                  <div>
                    <div
                      className="fourth-text"
                      style={{ fontFamily: 'Editorial New' }}
                    >
                      Even Brighter
                    </div>
                  </div>
                </div>
                <div className={`${styles['content']} fourth-text`}>
                  The surrounding area of Millgrove has vastly improved over the
                  last few years and is becoming increasingly prime. On the
                  forthcoming completion of nearby infrastructure projects, the
                  estate will be connected like no other.
                </div>
              </div>
              <div className={`${styles['potential-image']} fourth-text`}>
                <img
                  src={`${cloudFrontUrl}/toy-plane.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
                <div className="tag md grey"> Lifestyle Image </div>
              </div>
            </div>
          </section>
          <section style={{ backgroundColor: '#f9f6ed' }}>
            <ComfortCarousel
              primaryColor={'#f9f6ed'}
              secondaryColor={'#434f50'}
              slidesList={newLocationSlides}
              isLocation={true}
            />
          </section>
          <section className={styles['footer']}>
            <div className={styles.theHomeSection}>
              <div className={styles.homeSectionStyle}>
                <div className={styles.groupSec}>
                  <div className={styles.headSec}>
                    <span className={styles.textNotera}>The</span>
                    <div className={`${styles.titleCustom}`}>Grounds</div>
                  </div>

                  {/* Hide on Desktop */}
                  <button
                    onClick={() => router.push('/section/grounds')}
                    className={styles.discoverBtnMobile}
                    ref={mobileBtnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                </div>

                <div className={styles.assetsSec}>
                  <button
                    onClick={() => router.push('/section/grounds')}
                    className={styles.discoverBtn}
                    ref={btnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                  <div
                    className={styles.imgBoxStyle}
                    data-aos="fade-up"
                    data-aos-duration="1000"
                  >
                    <img
                      src={`${cloudFrontUrl}/grounds-main-small.webp`}
                      alt="millgrove properties site"
                      className={styles.imgStyle}
                    />
                    <div className={`tag md up ${isMobile || isDesktop ? 'grey' : 'darkGreen'}`}> Artistic Impression </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </section>
        </div>
      </AnimatedLayout>
    </PrivateRoute>
  );
};

export default Location;
