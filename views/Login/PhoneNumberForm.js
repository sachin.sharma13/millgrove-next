import React, { useEffect, useRef, useState } from 'react';
import Button from '../../components/Button';
import { MILLGROVE_TREE } from '../../utils/assets';
import styles from './Login.module.scss';
import { verifyPhoneNumber } from './Login.helpers';
import { WarningOctagon } from '../../public/icons/icons';
import { checkNonNumericInput } from '../../utils/checkNonNumericInput';
import { isPhoneNosValid } from '../../utils/isPhoneNosValid';
import CountrySelect from '../../components/CountrySelect';

const PhoneNumberForm = ({
  setIsEnteringPhoneNos,
  setIsEnteringOtp,
  setOtpToken,
  setIsRegistering,
  setIsLoggingIn,
  phoneNos,
  setPhoneNos,
  selectedCountry,
  setSelectedCountry,
}) => {
  const [error, setError] = useState({ errorOccured: false, msg: '' });
  const inputRef = useRef();

  const continueHandler = async (e) => {
    e.preventDefault();
    if (phoneNos?.length === 10) {
      try {
        const res = await verifyPhoneNumber(
          `+${selectedCountry.dial_code}${phoneNos}`
        );
        if (res?.success) {
          setOtpToken(res.otpToken);
          setIsEnteringPhoneNos(false);
          setIsEnteringOtp(true);
        } else {
          setError((prev) => ({
            ...prev,
            errorOccured: true,
            msg: `${res.errorMsg}`,
          }));
        }
      } catch (err) {
        console.log(err);
        setError((prev) => ({
          ...prev,
          errorOccured: true,
          msg: `${err?.response?.data?.message},`,
        }));
      }
    } else {
    }
  };

  const handleChange = (val) => {
    if (val.length <= 10) {
      setPhoneNos(val);
      setError({ errorOccured: false, msg: '' });
    }
  };

  const goToSignupPage = () => {
    setIsRegistering(true);
    setIsLoggingIn(false);
  };

  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      continueHandler(e);
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', handleKeyPress);
    return () => document.removeEventListener('keydown', handleKeyPress);
  }, [phoneNos]);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <div className={styles.mainWrapper}>
      <form className={styles.formWrapper}>
        <div className={styles.cross} onClick={() => setIsLoggingIn(false)}>
          <img src="/images/cross.svg" alt="cross" />
        </div>
        <div className={styles.headSection}>
          <div className={styles.bgTree}>
            <MILLGROVE_TREE fillColor="#b1bbae" />
          </div>
          <div className={styles.headingWrapper}>
            <h3 className={styles.heading}>Enter Phone Number</h3>
          </div>
          <p className={styles.notRegisteredText}>
            Not registered yet? Please <a onClick={goToSignupPage}>register</a>
          </p>
        </div>
        <div className="input-relative-sec">
          <div className={styles.phoneNosWrapper}>
            <CountrySelect
              selectedCountry={selectedCountry}
              setSelectedCountry={setSelectedCountry}
              numberRef={inputRef}
            />
            <input
              ref={inputRef}
              className={styles.phoneNumberInput}
              type={'text'}
              placeholder="Phone Number"
              value={phoneNos}
              onChange={(e) => handleChange(e.target.value)}
              onKeyDown={(e) => checkNonNumericInput(e)}
              inputMode="numeric"
            />
            {error.errorOccured ? (
              <span className={styles.warningIconWrapper}>
                <WarningOctagon />
              </span>
            ) : null}
          </div>

          <div className="error-sec">
            <p className={styles.errorText}>
              {error.errorOccured ? error.msg : ''}
              {error.errorOccured && !error.msg.includes('review') && (
                <>
                  {' '}
                  Please <span onClick={goToSignupPage}>register</span>
                </>
              )}
            </p>
          </div>
        </div>
        <div className={styles.submitBtnWrapper}>
          <Button
            type="submit"
            clickhandler={(e) => continueHandler(e)}
            text={'Continue'}
            classname="button-style48"
            isDisabled={!isPhoneNosValid(phoneNos, selectedCountry.dial_code)}
          />
        </div>
      </form>
    </div>
  );
};

export default PhoneNumberForm;
