import gsap from 'gsap';
import { useEffect, useRef, useState } from 'react';
import { cloudFrontUrl } from '../../utils/constants';
import { animationGenrator } from '../../utils/assets';
import { useIntersection } from '../../hooks/useIntersection';
import { useIsMobile } from '../../hooks/useIsMobile';
import ReactSimplyCarousel from 'react-simply-carousel';

function Secondfold() {
  const carouselRef = useRef();
  const isCarouselVisible = useIntersection(carouselRef);
  const [isMobile] = useIsMobile();
  const [currentSlide, setCurrentSlide] = useState(0);

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger);
  //   setTimeout(() => {
  //     animationGenrator('.animation-secondfold', '#madeWithCare');
  //   }, 1000);
  // }, []);

  return (
    <div className="second-fold-container">
      <div className="secondfold-image-container" ref={carouselRef}>
        {isMobile ? (
          <ReactSimplyCarousel
            activeSlideIndex={currentSlide}
            onRequestChange={setCurrentSlide}
            speed={400}
            infinite={true}
            autoplay={isCarouselVisible}
            autoFocus={true}
            autoplayDelay={4000}
            swipeTreshold={100}
            backwardBtnProps={{
              style: {
                all: 'unset',
              },
            }}
            forwardBtnProps={{
              style: {
                all: 'unset',
              },
            }}
            dotsNav={{
              show: true,
              containerProps: {
                style: {
                  marginTop: '-20px',
                  zIndex: 2,
                  gap: '8px',
                  display: 'flex',
                },
              },
              itemBtnProps: {
                style: {
                  height: 8,
                  width: 8,
                  padding: 0,
                  borderRadius: '100%',
                  border: '1px solid white',
                  opacity: 0.5,
                },
              },
              activeItemBtnProps: {
                style: {
                  height: 8,
                  width: 8,
                  padding: 0,
                  borderRadius: '100%',
                  boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                  border: 0,
                  background: 'white',
                },
              },
            }}
          >
            <div className="carousel-image-container">
              <img
                src={`${cloudFrontUrl}/landing-page-banner.webp`}
                alt="banner"
                width={'100%'}
                height={'100%'}
              />
              <div className="tag md"> Artistic Impression </div>
            </div>
            <div className="carousel-image-container">
              <img
                src={`${cloudFrontUrl}/semi-aerial.webp`}
                alt="banner"
                width={'100%'}
                height={'100%'}
              />
              <div className="tag md"> Artistic Impression </div>
            </div>
          </ReactSimplyCarousel>
        ) : (
          <div
            className="parallax"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/landing-page-banner.webp)`,
            }}
          >
            <div className="tag"> Artistic Impression </div>
          </div>
        )}
      </div>
      <div className="mg-second-fold" id="madeWithCare">
        <div className="mg-second-fold-text">
          <div className="animation-block">
            <div className="mg-second-fold-heading animation-secondfold">
              Made <span>with</span> Care
            </div>
          </div>
          <div className="animation-block">
            <div className="mg-second-fold-subheading animation-secondfold">
              Our purpose is simple. To create somewhere we are proud to call
              home; somewhere we can leave a fitting legacy. We warmly invite
              you to join our small, intimate community, where neighbours are
              friends. Welcome to Millgrove - a limited collection of premium
              plots nestled in the heart of Gurugram.
            </div>
          </div>
          <div className="animation-block">
            <div className="mg-second-fold-footer animation-secondfold">
              Millgrove
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Secondfold;
