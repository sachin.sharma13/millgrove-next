import React, { useEffect, useRef, useState } from 'react';
import styles from './Homes.module.scss';
import Footer from '../Footer';
import { useRouter } from 'next/router';
import { AccordionCarousel } from '../../components/AccordionCarousel';
import { AccordionCarouselMobile } from '../../components/AccordionCarouselMobile';
import Header from '../../components/Header/Header';
import { cloudFrontUrl } from '../../utils/constants';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { animationGenrator, isSafariMobile } from '../../utils/assets';
import { useIntersection } from '../../hooks/useIntersection';
import PrivateRoute from '../../components/PrivateRoute';
import { useIsMobile } from '../../hooks/useIsMobile';
import ReactSimplyCarousel from 'react-simply-carousel';

const highlights = [
  'Freehold',
  'Low-Rise',
  'Custom-made',
  'Private Garden & Rooftop',
  '3m+ High Ceilings',
  'Layout Flexibility',
  'Elevator Provision',
  'Easier Vastu Compliance',
];

const Homes = () => {
  const router = useRouter();
  const [currentSlide, setCurrentSlide] = useState(0);
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const [isMobile] = useIsMobile();
  const firstCarouselRef = useRef();
  const isFirstCarouselVisible = useIntersection(firstCarouselRef);
  const secondCarouselRef = useRef();
  const isSecondCarouselVisible = useIntersection(secondCarouselRef);
  const thirdCarouselRef = useRef();
  const isThirdCarouselVisible = useIntersection(thirdCarouselRef);
  const [currentSlideFirst, setCurrentSlideFirst] = useState(0);
  const [currentSlideSecond, setCurrentSlideSecond] = useState(0);
  const [currentSlideThird, setCurrentSlideThird] = useState(0);

  useEffect(() => {
    animationGenrator('.first-text', '#firstSection');
    setTimeout(() => {
      animationGenrator('.second-text', '#secondSection');
      animationGenrator('.third-text', '#thirdSection');
      animationGenrator('.fourth-text', '#fourthSection');
      animationGenrator('.fifth-text', '#fifthSection');
      animationGenrator('.sixth-text', '#sixthSection');
      animationGenrator('.seventh-text', '#seventhSection');
    }, 1000);
  }, []);

  useEffect(() => {
    if (isVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => btnRef?.current?.classList.remove(styles.rotateMe);
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);

  return (
    <PrivateRoute>
      <AnimatedLayout>
        <div className={styles['homes-page']}>
          <Header isFooterVisible={isMobileBtnVisible || isVisible} />
          <section
            id="firstSection"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/homes-bg.webp)`,
              filter: 'saturate(1.22)',
            }}
            className={`${styles['main-image']} animation-block`}
          >
            <div className={`${styles['main-title']} first-text`}>HOMES</div>
          </section>
          <section
            id="secondSection"
            className={`${styles['second-section']} animation-block`}
          >
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="second-text">
                  Designed <span>for</span>
                </div>
                <div className="second-text">Modern Comfort</div>
              </div>
              <div className={`${styles['content']} second-text`}>
                Refined, elegant and intuitively planned, Millgrove homes are
                generously spacious yet warmly intimate. Floor-to-ceiling
                windows and glass sliding doors keep the living space aglow with
                natural light – and the airy, open-plan feel makes it an ideal
                place for get togethers. Relax with your closest ones watching
                movies, lunching in your private garden, or sipping sundowners
                on your secluded rooftop.
              </div>
            </div>
            <div className={`${styles['images-div']} second-text`}>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/modern-image-11.webp`}
                  alt="modern-image"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/entrance-lobby.webp`}
                  alt="modern-image"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/lfg_study_room.webp`}
                  alt="modern-image"
                />
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} second-text`}
              ref={thirdCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideThird}
                onRequestChange={setCurrentSlideThird}
                speed={400}
                infinite={true}
                autoplay={isThirdCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/entrance-lobby.webp`}
                    alt="modern-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/modern-image-11.webp`}
                    alt="modern-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/lfg_study_room.webp`}
                    alt="modern-image"
                    loading="lazy"
                  />
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <section
            id="fourthSection"
            className={`${styles['fourth-section']} animation-block`}
          >
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                {isMobile ? (
                  <div className="fourth-text">Beautiful Custom-built</div>
                ) : (
                  <div className="fourth-text">
                    Beautiful <span>Custom-built</span>
                  </div>
                )}
                <div className="fourth-text">homes made easy</div>
              </div>
              <div className={`${styles['content']} fourth-text`}>
                Living in a bespoke home has never been more convenient. Select
                between options for your façade, size, entrance style and
                outdoor space, and our trusted contractor will tailor the
                property to your exact specification. Millgrove will supply the
                structural drawings, supervise the construction and assure the
                prompt, quality delivery of your new exemplary home.
              </div>
            </div>
            <div className={styles['accordion-carousel']}>
              <AccordionCarousel
                styles={styles}
                title={'Facade'}
                features={{
                  Heath: [
                    `${cloudFrontUrl}/facade_heath1.webp`,
                    `${cloudFrontUrl}/facade_heath2.webp`,
                  ],
                  Meadow: [
                    `${cloudFrontUrl}/facade_meadow1.webp`,
                    `${cloudFrontUrl}/facade_meadow2.webp`,
                  ],
                  Orchard: [
                    `${cloudFrontUrl}/facade_orchard1.webp`,
                    `${cloudFrontUrl}/facade_orchard2.webp`,
                  ],
                }}
                currentSlide={currentSlide}
                setCurrentSlide={setCurrentSlide}
                index={0}
              />
              <AccordionCarousel
                styles={styles}
                title={'Entrance'}
                features={{
                  'Front Entrance': [`${cloudFrontUrl}/entrance_front.webp`],
                  'Side Entrance': [`${cloudFrontUrl}/entrance_side.webp`],
                }}
                currentSlide={currentSlide}
                setCurrentSlide={setCurrentSlide}
                index={1}
              />
              <AccordionCarousel
                styles={styles}
                title={'Size'}
                features={{
                  '3 FLOORS (>8000ft<sup>2</sup> &nbsp; CA)': [
                    `${cloudFrontUrl}/8000ft.webp`,
                  ],
                  '2 FLOORS (>5000ft<sup>2</sup> &nbsp; CA)': [
                    `${cloudFrontUrl}/5000ft.webp`,
                  ],
                }}
                currentSlide={currentSlide}
                setCurrentSlide={setCurrentSlide}
                index={2}
              />
              <AccordionCarousel
                styles={styles}
                title={'Outdoors'}
                features={{
                  Pool: [`${cloudFrontUrl}/outdoors_pool.webp`],
                  'Extended Garden': [`${cloudFrontUrl}/outdoors-garden.webp`],
                }}
                currentSlide={currentSlide}
                setCurrentSlide={setCurrentSlide}
                index={3}
              />
            </div>
            <AccordionCarouselMobile styles={styles} />
          </section>
          <section
            id="thirdSection"
            className={`${styles['third-section']} animation-block`}
          >
            <div>
              <div className={styles['text-div']}>
                <div className={styles['title']}>
                  <div className="third-text">
                    Realise {isMobile && <span>the</span>}
                  </div>
                  <div className="third-text">
                    {!isMobile && <span>the</span>}Potential
                  </div>
                </div>
                <div className={`${styles['content']} third-text`}>
                  Our virtual home brings to life just one of the endless layout
                  configurations possible at Millgrove. With an uncompromising
                  approach to design, every home has the potential to exceed the
                  wildest of expectations. The sizeable indoor-outdoor living
                  areas, and multiple rooms and entertainment spaces under one
                  roof mean that you can savour time spent with friends and
                  family, with all the privacy you need.
                </div>
              </div>
              <div className={styles['potential-image']}>
                <img
                  src={`${cloudFrontUrl}/basement-entertainment-img.webp`}
                  alt="potential-image"
                  loading="lazy"
                  className="third-text"
                />
              </div>
            </div>
            <div className={`${styles['explore-btn']} third-text`}>
              Explore Virtual Home
            </div>
            <p className={`${styles['coming-soon-btn']} third-text`}>
              Coming soon
            </p>
            <div className={styles['images-div-mobile']} ref={firstCarouselRef}>
              {isSafariMobile ? (
                <ReactSimplyCarousel
                  activeSlideIndex={currentSlideFirst}
                  onRequestChange={setCurrentSlideFirst}
                  speed={400}
                  infinite={true}
                  autoplay={isFirstCarouselVisible}
                  autoFocus={true}
                  autoplayDelay={4000}
                  swipeTreshold={100}
                  backwardBtnProps={{
                    style: {
                      all: 'unset',
                    },
                  }}
                  forwardBtnProps={{
                    style: {
                      all: 'unset',
                    },
                  }}
                  dotsNav={{
                    show: true,
                    containerProps: {
                      style: {
                        marginTop: '-20px',
                        zIndex: 2,
                        gap: '8px',
                        display: 'flex',
                      },
                    },
                    itemBtnProps: {
                      style: {
                        height: 8,
                        width: 8,
                        padding: 0,
                        borderRadius: '100%',
                        border: '1px solid white',
                        opacity: 0.5,
                      },
                    },
                    activeItemBtnProps: {
                      style: {
                        height: 8,
                        width: 8,
                        padding: 0,
                        borderRadius: '100%',
                        boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                        border: 0,
                        background: 'white',
                      },
                    },
                  }}
                >
                  <div className={styles['image-container']}>
                    <img
                      src={`${cloudFrontUrl}/basement-entertainment-img.webp`}
                      alt="modern-image"
                      width={'100%'}
                      height={'100%'}
                      loading="lazy"
                    />
                  </div>
                  <div className={styles['image-container']}>
                    <img
                      src={`${cloudFrontUrl}/homes-rooftop.webp`}
                      width={'100%'}
                      height={'100%'}
                      alt="modern-image"
                      loading="lazy"
                    />
                  </div>
                </ReactSimplyCarousel>
              ) : (
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/basement-entertainment-img.webp`}
                    alt="potential-image"
                    loading="lazy"
                    className="third-text"
                  />
                </div>
              )}
            </div>
          </section>
          <section
            id="fifthSection"
            className={`${styles['fifth-section']} animation-block`}
          >
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="fifth-text">A home for</div>
                <div className="fifth-text">every style</div>
              </div>
              <div className={`${styles['content']} fifth-text`}>
                The interior of your Millgrove home will be left as a blank
                canvas for you to make your own. The long structural spans offer
                an abundance of flexible space and countless internal layout
                possibilities. Express yourself and finish your new home to suit
                your personal taste, needs and living style.
              </div>
              <div className={`${styles['cta-btn']} fifth-text`}>
                LAYOUT INSPIRATION
              </div>
              <p className={`${styles['coming-soon-btn']} fifth-text`}>
                Coming soon
              </p>
            </div>
            <div className={`${styles['images-div']} fifth-text`}>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/style-image-1.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/homes-living room.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/style-image-3.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/style-image-4.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/homes-vanity.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/walk-in_wardrobe.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} fifth-text`}
              ref={secondCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideSecond}
                onRequestChange={setCurrentSlideSecond}
                speed={400}
                infinite={true}
                autoplay={isSecondCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/style-image-1.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/homes-living room.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/style-image-3.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/style-image-4.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/homes-vanity.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/walk-in_wardrobe.webp`}
                    alt="potential-image"
                    loading="lazy"
                  />
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <div
            className="parallax"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/homes-rooftop.webp)`,
              display: isSafariMobile ? 'none' : 'block',
            }}
          ></div>
          <section
            id="sixthSection"
            className={`${styles['sixth-section']} animation-block`}
          >
            <div>
              <div className={`${styles['potential-image']} sixth-text`}>
                <img
                  src={`${cloudFrontUrl}/front-garden-small.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
              <div className={styles['text-div']}>
                <div className={styles['title']}>
                  <div className="sixth-text">LOW-RISE HOMES</div>
                  <div className="sixth-text">
                    CRAFTED<span>with</span>PRECISION
                  </div>
                </div>
                <div className={`${styles['content']} sixth-text`}>
                  Each Millgrove home incorporates a quality blend of curated
                  materials. The double glazing has a luxuriously thin profile
                  and standout technical characteristics. The external façades
                  excel in terms of durability and visual appeal and comprise of
                  imported stone, special treated cladding, and premium textured
                  paints.
                </div>
              </div>
            </div>
            <div className={`${styles['images-div-mobile']} sixth-text`}>
              <div className={styles['image-container']}>
                <img
                  src={`${cloudFrontUrl}/front-garden-small.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
            </div>
          </section>
          <section
            id="seventhSection"
            className={`${styles['seventh-section']} animation-block`}
          >
            <div className={`${styles['title']} seventh-text`}>
              homes highlights
            </div>
            <div className={styles['content']}>
              <div className={styles['list']}>
                {highlights.map((item, index) => (
                  <div className="seventh-text" key={index}>
                    {item}
                  </div>
                ))}
              </div>
              <div className={`${styles['image-div']} seventh-text`}>
                <img
                  // height="200px"
                  // width="300px"
                  src={`${cloudFrontUrl}/homes-pooldeck.webp`}
                  alt="potential-image"
                  loading="lazy"
                />
              </div>
            </div>
          </section>
          <section className={styles['footer']}>
            <div className={styles.theHomeSection}>
              <div className={styles.homeSectionStyle}>
                <div className={styles.groupSec}>
                  <div className={styles.headSec}>
                    <span className={styles.textNotera}>The</span>
                    <div className={`${styles.titleCustom}`}>Comforts</div>
                  </div>
                  {/* Hide on Desktop */}
                  <button
                    onClick={() => router.push('/section/comforts')}
                    className={styles.discoverBtnMobile}
                    ref={mobileBtnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                </div>
                <div className={styles.assetsSec}>
                  <button
                    onClick={() => router.push('/section/comforts')}
                    className={styles.discoverBtn}
                    ref={btnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                  <div
                    className={styles.imgBoxStyle}
                    data-aos="fade-up"
                    data-aos-duration="1000"
                  >
                    <img
                      src={`${cloudFrontUrl}/couple-walking-smallest.webp`}
                      alt="millgrove properties site"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </section>
        </div>
      </AnimatedLayout>
    </PrivateRoute>
  );
};

// export default Homes;
export default () => <></>;
