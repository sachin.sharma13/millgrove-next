/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { useRouter } from 'next/router';
import { useRef } from 'react';
import { useIntersection } from '../../hooks/useIntersection';
import { useEffect } from 'react';
import { sliderData } from '../../utils/assets';

function HomeCarouselMobile() {
  useEffect(() => {
    let controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 0,
        duration: 1300,
        offset: -65,
      },
    });

    let slides = document.querySelectorAll('div.slider-item');
    for (let i = 0; i < slides.length; i++) {
      let animation = TweenMax.to(slides[i], 0.7, {
        opacity: 0,
        ease: Quad.easeInOut,
      });

      new ScrollMagic.Scene({
        triggerElement: slides[i],
      })
        .setPin(slides[i], { pushFollowers: false })
        .addTo(controller)
        .setTween(animation);
    }
  }, []);

  return (
    <div className="home-carousel-mobile">
      {sliderData.map((item, index) => (
        <MobilSlides item={item} index={index} />
      ))}
    </div>
  );
}

export default HomeCarouselMobile;

const MobilSlides = ({ item, index }) => {
  const router = useRouter();
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add('discoverBtnMobile-rotateMe');
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove('discoverBtnMobile-rotateMe');
    }
    return () =>
      mobileBtnRef?.current?.classList.remove('discoverBtnMobile-rotateMe');
  }, [isMobileBtnVisible]);

  return (
    <div
      className="slider-item"
      key={index}
      style={{ borderColor: item.bgColor }}
    >
      <div className="carousel-text-mobile">
        <div className="carousel-heading-mobile">{item.title}</div>
      </div>
      <div className="carousel-img-mobile">
        <img src={item.imgUrl} alt="" />
        <div className="tag md"> {item.imgTitle} </div>
      </div>
      <div className="carousel-subtext-mobile">{item.description}</div>
      <button
        onClick={() => router.push(`/section/${item?.title.toLowerCase()}`)}
        className={'discover-btn-mobile'}
        ref={mobileBtnRef}
      >
        <img
          src="/images/discover-webBlack.svg"
          alt="discover-logo"
          className={'img-style'}
          loading="lazy"
        />
        <img
          src="/images/discoverArrowBlack.svg"
          alt="discover-arrow"
          className={'img-style-arrow'}
        />
      </button>
    </div>
  );
};
