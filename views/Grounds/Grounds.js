import { useRouter } from 'next/router';
import Footer from '../Footer';
import styles from './Grounds.module.scss';
import { useEffect, useRef, useState } from 'react';
import Header from '../../components/Header/Header';
import { cloudFrontUrl } from '../../utils/constants';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { animationGenrator, isSafariMobile } from '../../utils/assets';
import { useIntersection } from '../../hooks/useIntersection';
import PrivateRoute from '../../components/PrivateRoute';
import ReactSimplyCarousel from 'react-simply-carousel';
import { useIsMobile } from '../../hooks/useIsMobile';
import { useIsDesktop } from '../../hooks/useIsDesktop';

const highlights = [
  '~5 acres of common greens',
  '4,500+ trees planted',
  '60,000+ groundcovers',
  '3km+ of sidewalks & paths',
  '14km of automated irrigation',
  'Wheelchair accessible',
];

const Grounds = () => {
  const router = useRouter();
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const firstCarouselRef = useRef();
  const isFirstCarouselVisible = useIntersection(firstCarouselRef);
  const secondCarouselRef = useRef();
  const isSecondCarouselVisible = useIntersection(secondCarouselRef);
  const thirdCarouselRef = useRef();
  const isThirdCarouselVisible = useIntersection(thirdCarouselRef);
  const [currentSlideFirst, setCurrentSlideFirst] = useState(0);
  const [currentSlideSecond, setCurrentSlideSecond] = useState(0);
  const [currentSlideThird, setCurrentSlideThird] = useState(0);
  const [showIframe, setShowIframe] = useState(false);
  const [showCross, setShowCross] = useState(false);
  const [isMobile] = useIsMobile();
  const [isDesktop] = useIsDesktop();

  useEffect(() => {
    animationGenrator('.first-text', '#firstSection');
    setTimeout(() => {
      animationGenrator('.second-text', '#secondSection');
      animationGenrator('.third-text', '#thirdSection');
      animationGenrator('.fourth-text', '#fourthSection');
      animationGenrator('.fifth-text', '#fifthSection');
    }, 1000);
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setShowCross(showIframe);
    }, 2000);
  }, [showIframe]);

  useEffect(() => {
    if (isVisible && btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      btnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => btnRef?.current?.classList.remove(styles.rotateMe);
  }, [isVisible]);

  useEffect(() => {
    if (isMobileBtnVisible && mobileBtnRef) {
      setTimeout(() => {
        mobileBtnRef?.current?.classList.add(styles.rotateMe);
      }, 3000);
    } else {
      mobileBtnRef?.current?.classList.remove(styles.rotateMe);
    }
    return () => mobileBtnRef?.current?.classList.remove(styles.rotateMe);
  }, [isMobileBtnVisible]);

  return (
    <PrivateRoute>
      <AnimatedLayout>
        <div className={styles['grounds-page']}>
          <Header isFooterVisible={isMobileBtnVisible || isVisible} />
          <section
            style={{
              backgroundImage: `url(${cloudFrontUrl}/grounds-main-img.webp)`,
              filter: 'saturate(1.22)',
            }}
            id="firstSection"
            className={`${styles['main-image']} animation-block`}
          >
            <div className={`${styles['main-title']} first-text`}>Grounds</div>
            <div className="tag bottom green"> Artistic Impression </div>
          </section>
          <section
            id="secondSection"
            className={`${styles['second-section']} animation-block`}
          >
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="second-text">
                  Your
                  <span className="second-text">own</span>
                </div>
                <div className="second-text">Private Oasis</div>
              </div>
              <div className={`${styles['content']} second-text`}>
                Millgrove is a lush sanctuary. Explore the acres of exclusive
                greens or simply rest, relax, and recharge in them. Whether
                losing yourself in a novel under the seductive shade of a tree,
                or reuniting with friends over a picnic, the grounds offer
                themselves up as the perfect respite for mind and body.
              </div>
            </div>
            <div className={`${styles['images-div']} second-text`}>
              <div className={styles['image-container']}>
                <img
                  className={styles['child-playing']}
                  src={`${cloudFrontUrl}/grounds-football_small.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['picnic-spot']}
                  src={`${cloudFrontUrl}/picnic_park.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                />
                <div className="tag md"> Artistic Impression</div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['yoga-girl']}
                  src={`${cloudFrontUrl}/grounds-yoga.webp`}
                  alt="oasis-image"
                  style={{ filter: 'saturate(1.22)' }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} second-text`}
              ref={firstCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideFirst}
                onRequestChange={setCurrentSlideFirst}
                speed={400}
                infinite={true}
                autoplay={isFirstCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/picnic_park.webp`}
                    alt="oasis-image"
                  />
                  <div className="tag"> Artistic Impression</div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/grounds-football.webp`}
                    alt="oasis-image"
                  />
                  <div className="tag"> Lifestyle Image</div>
                </div>
                <div className="image-container-carousel">
                  <img
                    src={`${cloudFrontUrl}/grounds-yoga.webp`}
                    alt="oasis-image"
                  />
                  <div className="tag"> Lifestyle Image</div>
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <section
            id="thirdSection"
            className={`${styles['third-section']} animation-block`}
          >
            <div>
              <div className={styles['text-div']}>
                <div className={styles['title']}>
                  <div>
                    <span className="third-text">Sometimes the</span>
                  </div>
                  <div className="third-text">grass is greener</div>
                </div>
                <div className={`${styles['content']} third-text`}>
                  Millgrove is zero discharge by design. All rainwater is
                  harvested to recharge the ground aquifer, and all wastewater
                  is recycled for irrigation to keep the grounds lush all year
                  round. Rainfall flows naturally into specially designed parks
                  ensuring that the homes and roads are kept clear from flooding
                  during monsoon.
                </div>
              </div>
              <div className={`${styles['potential-image']} third-text`}>
                <img
                  src={`${cloudFrontUrl}/grounds-grass.webp`}
                  alt="potential-image"
                />
                <div className="tag green"> Lifestyle Image </div>
              </div>
            </div>
            <div className={styles['explore-btn']}>explore grounds</div>
            <div
              className={`${styles['images-div-mobile']} third-text`}
              ref={secondCarouselRef}
            >
              {isSafariMobile ? (
                <ReactSimplyCarousel
                  activeSlideIndex={currentSlideSecond}
                  onRequestChange={setCurrentSlideSecond}
                  speed={400}
                  infinite={true}
                  autoplay={isSecondCarouselVisible}
                  autoFocus={true}
                  autoplayDelay={4000}
                  swipeTreshold={100}
                  backwardBtnProps={{
                    style: {
                      all: 'unset',
                    },
                  }}
                  forwardBtnProps={{
                    style: {
                      all: 'unset',
                    },
                  }}
                  dotsNav={{
                    show: true,
                    containerProps: {
                      style: {
                        marginTop: '-20px',
                        zIndex: 2,
                        gap: '8px',
                        display: 'flex',
                      },
                    },
                    itemBtnProps: {
                      style: {
                        height: 8,
                        width: 8,
                        padding: 0,
                        borderRadius: '100%',
                        border: '1px solid white',
                        opacity: 0.5,
                      },
                    },
                    activeItemBtnProps: {
                      style: {
                        height: 8,
                        width: 8,
                        padding: 0,
                        borderRadius: '100%',
                        boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                        border: 0,
                        background: 'white',
                      },
                    },
                  }}
                >
                  <div className={styles['image-container']}>
                    <img
                      src={`${cloudFrontUrl}/grounds-grass.webp`}
                      alt="grass"
                      width={'100%'}
                      height={'100%'}
                    />
                    <div className="tag green"> Lifestyle Image </div>
                  </div>
                  <div className={styles['image-container']}>
                    <img
                      src={`${cloudFrontUrl}/grounds-park-banner.webp`}
                      alt="park"
                      width={'100%'}
                      height={'100%'}
                    />
                    <div className="tag green"> Artistic Impression </div>
                  </div>
                </ReactSimplyCarousel>
              ) : (
                <div className={styles['image-container']}>
                  <img
                    src={`${cloudFrontUrl}/grounds-grass.webp`}
                    alt="grass"
                  />
                  <div className="tag green"> Lifestyle Image</div>
                </div>
              )}
            </div>
          </section>
          <section
            id="fourthSection"
            className={`${styles['fourth-section']} animation-block`}
          >
            <div className={styles['text-div']}>
              <div className={styles['title']}>
                <div className="fourth-text">
                  {' '}
                  Just <span>as</span>
                </div>
                <div className="fourth-text">Nature Intended</div>
              </div>
              <div className={`${styles['content']} fourth-text`}>
                Millgrove is home to a vibrant array of wildlife. It’s a place
                where birds serenade with their morning-song; butterflies
                flutter from flower to flower; and leaves whistle in the breeze.
                An assortment of native trees and plants have been introduced
                throughout the grounds to kindle a flourishing natural
                ecosystem.
              </div>
              <div
                onClick={() => setShowIframe(true)}
                className={`${styles['cta-btn']} fourth-text`}
              >
                Explore The Grounds
              </div>
            </div>
            <div className={`${styles['images-div']} fourth-text`}>
              <div className={styles['image-container']}>
                <img
                  className={styles['girl-drinking-water']}
                  src={`${cloudFrontUrl}/grounds-grass-1.webp`}
                  alt="grass-image"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['bird-image']}
                  src={`${cloudFrontUrl}/bird-image.webp`}
                  alt="grass-image"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['bee-image']}
                  src={`${cloudFrontUrl}/bee-image.webp`}
                  alt="grass-image"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['butterfly-image']}
                  src={`${cloudFrontUrl}/grounds-nature.webp`}
                  alt="grass-image"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md bottom"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['old-men-laughing']}
                  src={`${cloudFrontUrl}/old-men-laughing.webp`}
                  alt="old-men"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
              <div className={styles['image-container']}>
                <img
                  className={styles['dad_carrying_son']}
                  src={`${cloudFrontUrl}/dad_carrying_son.webp`}
                  alt="dad-son"
                  style={{
                    filter: 'saturate(1.22)',
                  }}
                />
                <div className="tag md"> Lifestyle Image </div>
              </div>
            </div>
            <div
              className={`${styles['images-div-mobile']} fourth-text`}
              ref={thirdCarouselRef}
            >
              <ReactSimplyCarousel
                activeSlideIndex={currentSlideThird}
                onRequestChange={setCurrentSlideThird}
                speed={400}
                infinite={true}
                autoplay={isThirdCarouselVisible}
                autoFocus={true}
                autoplayDelay={4000}
                swipeTreshold={100}
                backwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                forwardBtnProps={{
                  style: {
                    all: 'unset',
                  },
                }}
                dotsNav={{
                  show: true,
                  containerProps: {
                    style: {
                      marginTop: '-20px',
                      zIndex: 2,
                      gap: '8px',
                      display: 'flex',
                    },
                  },
                  itemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      border: '1px solid white',
                      opacity: 0.5,
                    },
                  },
                  activeItemBtnProps: {
                    style: {
                      height: 8,
                      width: 8,
                      padding: 0,
                      borderRadius: '100%',
                      boxShadow: '1px 1px 2px rgba(0,0,0,.9)',
                      border: 0,
                      background: 'white',
                    },
                  },
                }}
              >
                <div className="image-container-carousel">
                  <img
                    className={styles['bee-image']}
                    src={`${cloudFrontUrl}/bee-image.webp`}
                    alt="grass-image"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['old-men-laughing']}
                    src={`${cloudFrontUrl}/old-men-laughing.webp`}
                    alt="old-men"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['bird-image']}
                    src={`${cloudFrontUrl}/bird-image.webp`}
                    alt="grass-image"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['butterfly-image']}
                    src={`${cloudFrontUrl}/grounds-nature.webp`}
                    alt="grass-image"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['girl-drinking-water']}
                    src={`${cloudFrontUrl}/grounds-grass-1.webp`}
                    alt="grass-image"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
                <div className="image-container-carousel">
                  <img
                    className={styles['dad_carrying_son']}
                    src={`${cloudFrontUrl}/dad_carrying_son.webp`}
                    alt="grass-image"
                  />
                  <div className="tag "> Lifestyle Image </div>
                </div>
              </ReactSimplyCarousel>
            </div>
          </section>
          <div
            className="parallax"
            style={{
              backgroundImage: `url(${cloudFrontUrl}/grounds-park-banner.webp)`,
              display: isSafariMobile ? 'none' : 'block',
            }}
          >
            <div className="tag"> Artistic Impression </div>
          </div>

          <section
            id="fifthSection"
            className={`${styles['fifth-section']} animation-block`}
          >
            <div className={`${styles['title']} fifth-text`}>
              grounds highlights
            </div>
            <div className={styles['content']}>
              <div className={styles['list']}>
                {highlights.map((item, index) => (
                  <div className="fifth-text" key={index}>
                    {item}
                  </div>
                ))}
              </div>
              <div className={`${styles['image-div']} fifth-text`}>
                <img
                  src={`${cloudFrontUrl}/grounds-park.webp`}
                  alt="potential-image"
                />
                <div className="tag md green"> Artistic Impression </div>
              </div>
            </div>
          </section>
          <section className={styles['footer']}>
            <div className={styles.theHomeSection}>
              <div className={styles.homeSectionStyle}>
                <div className={styles.groupSec}>
                  <div className={styles.headSec}>
                    <span className={styles.textNotera}>The</span>
                    <div className={`${styles.titleCustom}`}>Comforts</div>
                  </div>
                  {/* Hide on Desktop */}
                  <button
                    onClick={() => router.push('/section/comforts')}
                    className={styles.discoverBtnMobile}
                    ref={mobileBtnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                </div>
                <div className={styles.assetsSec}>
                  <button
                    onClick={() => router.push('/section/comforts')}
                    className={styles.discoverBtn}
                    ref={btnRef}
                  >
                    <img
                      src="/images/discover-web.svg"
                      alt="discover-logo"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <img
                      src="/images/discoverArrow.svg"
                      alt="discover-logo"
                      className={styles.imgStyleArrow}
                    />
                  </button>
                  <div
                    className={styles.imgBoxStyle}
                    data-aos="fade-up"
                    data-aos-duration="1000"
                  >
                    <img
                      src={`${cloudFrontUrl}/couple-walking-smallest.webp`}
                      alt="millgrove properties site"
                      className={styles.imgStyle}
                      loading="lazy"
                    />
                    <div className={`tag md up ${isMobile || isDesktop ? 'green' : 'darkGreen'}`}> Lifestyle Image </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </section>
          {showIframe ? (
            <>
              <iframe
                className={styles['iframe']}
                title="Explore grounds page"
                src="https://tours.millgrove.in/"
              ></iframe>
              {showCross && (
                <div
                  className={styles['cross-container']}
                  onClick={() => setShowIframe(false)}
                >
                  <img src="/images/cross.svg" alt="cross" />
                </div>
              )}
              <div className="warning-container portrait">
                <img width="200px" src="/images/portrait-rotate.webp" />
                <p>Please rotate your device</p>
              </div>
            </>
          ) : (
            <div className="warning-container landscape">
              <img src="/images/landscape-rotate.webp" />
              <p>Please rotate your device</p>
            </div>
          )}
        </div>
      </AnimatedLayout>
    </PrivateRoute>
  );
};

export default Grounds;
