import { Modal } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { AuthContext } from '../../context/AuthContext';
import { LEFT_ARROW, MILLGROVE_LOGO, RIGHT_ARROW } from '../../utils/assets';
import { apiKey, baseUrl, cloudFrontUrl } from '../../utils/constants';
import RequestSubmittedCard from '../../views/Reservation/RequestSubmittedCard';
import ReservationForm from '../../views/Reservation/ReservationForm';
import styles from './Header.module.scss';
import TreePrint from './TreePrint';
import { MILLGROVE_TREE } from '../../utils/assets';
import ReactSimplyCarousel from 'react-simply-carousel';
import axios from 'axios';
import { useIsMobile } from '../../hooks/useIsMobile';
import { useInitialRender } from '../../hooks/useInitialRender';
import { useIsDesktop } from '../../hooks/useIsDesktop';

const menuImages = {
  location: `${cloudFrontUrl}/location-banner.webp`,
  grounds: `${cloudFrontUrl}/grounds-main-img.webp`,
  // homes: `${cloudFrontUrl}/homes-bg.webp`,
  comforts: `${cloudFrontUrl}/couple-walking-small.webp`,
  legacy: `${cloudFrontUrl}/legacy-banner-latest.webp`,
};

const imageTitles = {
  location: 'Actual Photo',
  grounds: 'Artistic Impression',
  // homes: 'Actual Photo',
  comforts: 'Lifestyle Image',
  legacy: 'Lifestyle Image',
};

const menuOption = Object.keys(menuImages);

const Header = ({ isLandingPage, isFooterVisible }) => {
  const navBar = useRef(null);
  const [showMenu, setShowMenu] = useState(false);
  const router = useRouter();
  const [mainImage, setMainImage] = useState(
    menuImages[router.query.id] || menuImages[menuOption[0]]
  );
  const [imageTitle, setImageTitle] = useState(
    imageTitles[router.query.id] || imageTitles[menuOption[0]]
  );
  const [isMobile] = useIsMobile();
  const [isDesktop] = useIsDesktop();
  const [showContent, setShowContent] = useState(false);
  const [isHover, setIsHover] = useState(false);
  const [showReservationForm, setShowReservationForm] = useState(false);
  const [showRequestForm, setShowRequestForm] = useState(false);
  const [documentLinks, setdocumentLinks] = useState([]);
  const headerRef = useRef(null);
  const {
    userToken,
    logoutUser,
    userDetails: { visitRequested },
  } = useContext(AuthContext);
  const [currentSlide, setCurrentSlide] = useState(0);
  const [isInitialRender] = useInitialRender();
  const positionRef = useRef(0);

  useEffect(() => {
    if (showMenu) {
      positionRef.current =
        window.pageYOffset || document.documentElement.scrollTop;
      document.body.style.overflow = 'hidden';
      document.documentElement.style.overflow = 'hidden';
      document.body.style.position = 'fixed';
      document.body.style.top = `-${positionRef.current}px`;
    } else {
      document.body.style.overflow = '';
      document.documentElement.style.overflow = '';
      document.body.style.position = '';
      window.scrollTo(0, positionRef.current);
    }
  }, [showMenu]);

  useEffect(() => {
    showMenu &&
      navBar.current.addEventListener(
        'wheel',
        (e) => {
          e.preventDefault();
        },
        { passive: false }
      );
  }, [showMenu]);

  useEffect(
    () =>
      setTimeout(() => {
        setShowContent(showMenu);
      }, 200),
    [showMenu]
  );

  useEffect(() => {
    if (!window) return;
    if (isInitialRender) return;
    window.onscroll = () => {
      if (!headerRef.current) return;
      let currentScrollPos = window.pageYOffset;
      if (currentScrollPos > 200) {
        headerRef.current.style.backgroundColor = '#4B5B47';
        setShowRequestForm(true);
      }
      if (currentScrollPos <= 200) {
        headerRef.current.style.backgroundColor = 'unset';
        setShowRequestForm(false);
      }
    };
  }, [isLandingPage, isInitialRender]);

  useEffect(() => {
    if (!isFooterVisible) return;
    headerRef.current.style.backgroundColor = '#4B5B47';
    setShowRequestForm(true);
  }, [isFooterVisible]);

  useEffect(() => {
    if (userToken) {
      (async () => {
        try {
          const res = await axios.get(
            `${baseUrl}/client/get-files?limit=50&offset=0`,
            {
              headers: {
                'rest-api-key': apiKey,
                Authorization: `Bearer ${userToken}`,
              },
            }
          );
          setdocumentLinks(res.data.data.fileList);
        } catch (error) {
          if (error?.response?.request?.status === 401) {
            logoutUser();
          }
        }
      })();
    }
  }, [userToken]);

  return (
    <header ref={headerRef} className={styles['header']}>
      {userToken && (
        <div
          onClick={() => setShowMenu(true)}
          className={styles['menu-bar-web']}
        >
          <img src="/images/menu-icon.png" alt="none" />
        </div>
      )}
      {!(isDesktop || !showRequestForm) ? (
        <div
          onClick={() => setShowReservationForm(true)}
          className={styles['requestButton']}
        >
          REQUEST VISIT
        </div>
      ) : (
        <div
          onClick={() => userToken && router.push('/home')}
          className={`${userToken ? styles['companyLogo'] : undefined}`}
        >
          <MILLGROVE_LOGO />
        </div>
      )}
      {userToken && (
        <div
          onClick={() => setShowMenu(true)}
          className={styles['menu-bar-mobile']}
        >
          <img src="/images/menu.svg" alt="none" />
        </div>
      )}
      {showRequestForm ? (
        <div
          onClick={() => setShowReservationForm(true)}
          className={styles['requestButton']}
        >
          REQUEST VISIT
        </div>
      ) : (
        <div className={styles['reraDetails']}>
          <div>RERA REGISTRATION NO.:</div>
          <div>RC/REP/HARERA/GGM/696/428/2023/40</div>
          <a
            className="siteUrl hover-underline-animation"
            href={'https://haryanarera.gov.in/'}
            target="#blank"
          >
            www.haryanarera.gov.in
          </a>
        </div>
      )}
      {(isMobile || showMenu) && (
        <div
          style={
            isMobile
              ? showMenu
                ? { minWidth: '100%', left: 0, top: 0 }
                : { left: 0, minWidth: '0%' }
              : {}
          }
          ref={navBar}
          className={`${styles['navigation-menu']}`}
        >
          <div>
            {(showContent || isMobile) && (
              <div className={styles['fade-in']}>
                <header className={styles['header-nav']}>
                  <div
                    className={styles['companyLogo']}
                    onClick={() => {
                      router.pathname === '/home'
                        ? setShowMenu(false)
                        : router.push(`/home`);
                    }}
                  >
                    <MILLGROVE_LOGO className={styles['companyLogo']} />
                  </div>
                  <div
                    className={styles['cross']}
                    onClick={() => setShowMenu(false)}
                  >
                    <img src="/images/cross.svg" alt="cross" />
                  </div>
                </header>
                <div className={styles['main-content']}>
                  <div
                    onMouseEnter={() => setIsHover(true)}
                    onMouseLeave={() => {
                      setIsHover(false);
                      setMainImage(
                        menuImages[router.query.id] || menuImages[menuOption[0]]
                      );
                    }}
                  >
                    {menuOption.map((item, index) => (
                      <div
                        key={index}
                        onMouseEnter={() => {
                          setMainImage(menuImages[item]);
                          setImageTitle(imageTitles[item]);
                        }}
                        onClick={() => {
                          router.query.id === item
                            ? setShowMenu(false)
                            : router.push(`/section/${item}`);
                        }}
                        className={styles['page-name']}
                      >
                        {item}
                      </div>
                    ))}
                  </div>
                  <div className={styles['second-container']}>
                    <div className={styles['main-image']}>
                      <img
                        src={mainImage}
                        alt="main-img"
                        width="100%"
                        height="100%"
                      />
                      <div className="tag"> {imageTitle} </div>
                    </div>
                    <div className={styles['documents-container']}>
                      <h3 className={styles['title']}>Useful documents</h3>
                      <div style={{ position: 'relative' }}>
                        <ReactSimplyCarousel
                          activeSlideIndex={currentSlide}
                          onRequestChange={setCurrentSlide}
                          itemsToShow={1}
                          speed={400}
                          infinite={true}
                          backwardBtnProps={{
                            style: {
                              all: 'unset',
                              position: 'absolute',
                              left: '0',
                              top: '10px',
                            },
                            children: (
                              <span className={styles['arrow-container']}>
                                <LEFT_ARROW
                                  className={styles['left-arrow']}
                                  fillColor={'#9C7E61'}
                                />
                              </span>
                            ),
                          }}
                          forwardBtnProps={{
                            style: {
                              all: 'unset',
                              position: 'absolute',
                              right: '0',
                              top: '10px',
                            },
                            children: (
                              <span className={styles['arrow-container']}>
                                <RIGHT_ARROW
                                  className={styles['right-arrow']}
                                  fillColor={'#9C7E61'}
                                />
                              </span>
                            ),
                          }}
                        >
                          {documentLinks.map(({ title, fileLink, _id }) => (
                            <a
                              href={fileLink}
                              target="_blank"
                              className={styles['document-wrapper']}
                            >
                              <div
                                key={_id}
                                className={styles['single-document']}
                              >
                                <div className={styles['circle']}>
                                  <MILLGROVE_TREE
                                    style={{
                                      height: '100%',
                                      width: '100%',
                                      objectFit: 'contain',
                                    }}
                                    fillColor={'#EBE5D3'}
                                  />
                                </div>
                                <div className={styles['title']}>{title}</div>
                                <div className={styles['img-container']}>
                                  <img
                                    src="/images/download-button.svg"
                                    alt="download"
                                  />
                                </div>
                              </div>
                            </a>
                          ))}
                        </ReactSimplyCarousel>
                      </div>
                    </div>
                  </div>
                </div>
                {isMobile && showMenu && (
                  <div className={styles['documents-container']}>
                    <h3 className={styles['title']}>Useful documents</h3>
                    <div style={{ position: 'relative' }}>
                      <ReactSimplyCarousel
                        activeSlideIndex={currentSlide}
                        onRequestChange={setCurrentSlide}
                        itemsToShow={1}
                        speed={400}
                        infinite={true}
                        backwardBtnProps={{
                          style: {
                            all: 'unset',
                            position: 'absolute',
                            left: '0',
                            top: '10px',
                          },
                          children: (
                            <span className={styles['arrow-container']}>
                              <LEFT_ARROW
                                className={styles['left-arrow']}
                                fillColor={'#9C7E61'}
                              />
                            </span>
                          ),
                        }}
                        forwardBtnProps={{
                          style: {
                            all: 'unset',
                            position: 'absolute',
                            right: '0',
                            top: '10px',
                          },
                          children: (
                            <span className={styles['arrow-container']}>
                              <RIGHT_ARROW
                                className={styles['right-arrow']}
                                fillColor={'#9C7E61'}
                              />
                            </span>
                          ),
                        }}
                      >
                        {documentLinks.map(({ title, fileLink, _id }) => (
                          <a
                            href={fileLink}
                            target="_blank"
                            className={styles['document-wrapper']}
                          >
                            <div
                              key={_id}
                              className={styles['single-document']}
                            >
                              <div className={styles['circle']}>
                                <MILLGROVE_TREE
                                  style={{
                                    height: '100%',
                                    width: '100%',
                                    objectFit: 'contain',
                                  }}
                                  fillColor={'#EBE5D3'}
                                />
                              </div>
                              <div className={styles['title']}>{title}</div>
                              <div className={styles['img-container']}>
                                <img
                                  src="/images/download-button.svg"
                                  alt="download"
                                />
                              </div>
                            </div>
                          </a>
                        ))}
                      </ReactSimplyCarousel>
                    </div>
                  </div>
                )}
                <div className={styles['reraDetailsMobile']}>
                  RERA REGISTRATION NO.: RC/REP/HARERA/GGM/696/428/2023/40
                  <a
                    className={` ${styles['siteUrl']}`}
                    href={'https://haryanarera.gov.in/'}
                    target="#blank"
                  >
                    www.haryanarera.gov.in
                  </a>
                </div>
              </div>
            )}

            <div>
              <img
                className={styles['background-image']}
                src="/images/Union.svg"
                alt="background-img"
              />
            </div>
          </div>
        </div>
      )}
      <Modal
        open={showReservationForm}
        onClose={() => setShowReservationForm(false)}
        style={{ display: 'flex', alignItems: 'center' }}
      >
        <div
          className={styles['reservationCard']}
          data-aos="fade-up"
          data-aos-duration="1000"
        >
          <div className={styles['treeSection']}>
            <TreePrint className={styles['treePrint']} />
          </div>
          {visitRequested ? <RequestSubmittedCard /> : <ReservationForm />}
          <div
            className={styles['cross-reservationForm']}
            onClick={() => setShowReservationForm(false)}
          >
            <img src="/images/cross.svg" alt="cross" />
          </div>
        </div>
      </Modal>
    </header>
  );
};

export default Header;
