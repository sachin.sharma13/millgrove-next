import React, { useEffect, useRef, useState } from 'react';
import styles from './Legacy.module.scss';
import { useIntersection } from '../../hooks/useIntersection';
import { useIsMobile } from '../../hooks/useIsMobile';

const YearDetails = ({ year, description, index }) => {
  return (
    <div className={styles['timeline-description']}>
      <h2 className={'third-text' + index}>{year}</h2>
      <p className={'third-text' + index}>{description}</p>
    </div>
  );
};

const TimelineDash = ({ isLastSlide, isVisible, slideHeight }) => {
  const isSeenAlready = useRef();
  const [isMobile] = useIsMobile();
  const timelineHeight = isSeenAlready?.current
    ? isMobile
      ? `${90 + slideHeight}px`
      : `${230 + slideHeight}px`
    : 0;
  useEffect(() => {
    if (isVisible) {
      if (typeof isSeenAlready?.current) {
        isSeenAlready.current = true;
      }
    }
  }, [isVisible, isSeenAlready]);
  return (
    <div className={styles['timeline-dash']}>
      {!isLastSlide ? (
        <div
          style={{
            height: timelineHeight,
            transition: 'height 2000ms ease',
          }}
        ></div>
      ) : null}
    </div>
  );
};

const TimelineMedia = ({ media, index, imgTitle }) => {
  const getPosterImg = (media) => {
    if (media.includes('begins'))
      return `${process.env.NEXT_PUBLIC_CLOUDFRONT_URL}/2022-thumbnail.webp`;
    if (media.includes('conclusion')) {
      return `${process.env.NEXT_PUBLIC_CLOUDFRONT_URL}/2017-small-thumbnail.webp`;
    }
  };
  return (
    <div className={styles['timeline-image-container']}>
      {media.includes('mp4') ? (
        <video
          poster={getPosterImg(media)}
          controls
          playsInline
          loop
          preload="auto"
          className={`${'third-text' + index} lazy`}
        >
          <source src={media} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      ) : (
        <>
          <img
            className={'third-text' + index}
            src={media}
            style={{ filter: 'saturate(1.22)' }}
          />
          <div className={'tag md' + ' ' + 'third-text' + index}>
            {' '}
            {imgTitle}{' '}
          </div>
        </>
      )}
    </div>
  );
};

const TimelineComponent = ({
  media,
  year,
  description,
  isLastSlide,
  setElementInView,
  index,
  imgTitle,
}) => {
  const boxRef = useRef();
  const isVisible = useIntersection(boxRef);
  useEffect(() => {
    if (isVisible) {
      setElementInView(year);
    }
  }, [isVisible]);
  return (
    <>
      <div
        id={'thirdSection' + index}
        key={year}
        ref={boxRef}
        className={styles['timeline-component']}
      >
        <YearDetails index={index} year={year} description={description} />
        <TimelineDash
          isVisible={isVisible}
          isLastSlide={isLastSlide}
          slideHeight={boxRef?.current?.clientHeight}
        />
        <TimelineMedia media={media} index={index} imgTitle={imgTitle} />
      </div>
    </>
  );
};

const TimeScaleItem = ({ year, elementInView }) => {
  return (
    <div className={styles['bar-item']}>
      <span
        className={`${styles['year']} ${
          Number(elementInView) === Number(year) ? styles['highlight'] : ''
        }`}
      >
        {year}
      </span>
      <span
        className={`${styles['dash']} ${
          Number(elementInView) === Number(year) ? styles['stretch'] : ''
        }`}
      ></span>
    </div>
  );
};

const Timeline = ({ timelineData }) => {
  const [elementInView, setElementInView] = useState(1992);

  return (
    <>
      <div className={styles['timescale']}>
        {timelineData.map(({ year }) => (
          <TimeScaleItem key={year} year={year} elementInView={elementInView} />
        ))}
      </div>
      <div className={styles['timeline-container']}>
        {timelineData.map(({ media, year, description, imgTitle }, index) => (
          <TimelineComponent
            key={year}
            media={media}
            year={year}
            description={description}
            isLastSlide={index === timelineData.length - 1}
            setElementInView={setElementInView}
            index={index}
            imgTitle={imgTitle}
          />
        ))}
      </div>
    </>
  );
};

export default Timeline;
