import axios from 'axios';
import Button from '../../components/Button';
import { AuthContext } from '../../context/AuthContext';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { MILLGROVE_TREE } from '../../utils/assets';
import styles from './Login.module.scss';
import Timer from './Timer';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { verifyPhoneNumber } from './Login.helpers';
import CustomOtp from '../../components/CustomOtp/CustomOtp';

const OtpForm = ({ setIsLoggingIn, otpToken, phoneNos, setOtpToken }) => {
  const [otp, setOtp] = useState('');
  const [seconds, setSeconds] = useState(59);
  const [error, setError] = useState({ errorOccured: false, msg: '' });
  const otpWrapperRef = useRef();
  const { setIsLoggedIn, loginWithCredentials } = useContext(AuthContext);
  const router = useRouter();

  const handleSubmit = async () => {
    try {
      const res = await loginWithCredentials(otp, otpToken);
      if (res?.success) {
        router.push('/home');
        setIsLoggingIn(false);
        setIsLoggedIn(true);
      } else {
        setError((prev) => ({
          ...prev,
          errorOccured: true,
          msg: `${res.errorMsg}`,
        }));
      }
    } catch (err) {
      console.log(err);
    }
  };

  const resendOtp = async (e) => {
    if (seconds > 0) return;
    try {
      setOtp('');
      const res = await verifyPhoneNumber(phoneNos);
      if (res?.success) {
        setOtpToken(res.otpToken);
        setSeconds(59);
        setOtp('');
        setError({ errorOccured: false, msg: '' });
      } else {
        setError((prev) => ({
          ...prev,
          errorOccured: true,
          msg: `${res.errorMsg}`,
        }));
      }
    } catch (err) {
      console.log(err);
    }
  };
  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      handleSubmit();
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', handleKeyPress);
    return () => document.removeEventListener('keydown', handleKeyPress);
  }, [otp]);

  return (
    <div className={styles.otpMainWrapper}>
      <div className={styles.bgTree}>
        <MILLGROVE_TREE style={{ height: '200' }} fillColor="#b1bbae" />
      </div>
      <div className={styles.otpFormWrapper}>
        <div className={styles.cross} onClick={() => setIsLoggingIn(false)}>
          <img src="/images/cross.svg" alt="cross" />
        </div>
        <div className={styles.headingWrapper}>
          <h3 className={styles.heading}>Enter One time password</h3>
        </div>
        <div ref={otpWrapperRef} className={styles.otpWrapper}>
          <CustomOtp otp={otp} setOtp={setOtp} otpLength={6} />
        </div>

        <div
          className="error-sec"
          style={{ position: 'relative', top: '24px' }}
        >
          <p className={styles.errorText}>
            {error.errorOccured ? error.msg : ''}
          </p>
        </div>

        <p className={styles.resendMsg}>
          <>One time password valid for 10 minutes</>
          {seconds ? (
            <div>
              You will receive a new code in{' '}
              <Timer seconds={seconds} setSeconds={setSeconds} />
            </div>
          ) : (
            <div>
              <span
                style={
                  seconds > 0
                    ? {
                        cursor: 'not-allowed',
                        textDecoration: 'none',
                        color: 'gray',
                      }
                    : undefined
                }
                onClick={resendOtp}
                className={styles.resend}
              >
                Click here
              </span>{' '}
              to receive a new code{' '}
              <Timer seconds={seconds} setSeconds={setSeconds} />
            </div>
          )}
        </p>
        <div className={styles.submitBtnWrapper}>
          <Button
            type="submit"
            clickhandler={() => otp?.length === 6 && handleSubmit()}
            text={'Continue'}
            isDisabled={otp?.length === 6 ? false : true}
            classname={`${styles.otpBtnStyle} button-style48`}
          />
        </div>
      </div>
    </div>
  );
};

export default OtpForm;
