/* eslint-disable @next/next/no-img-element */
import gsap from 'gsap';
import { useEffect, useState } from 'react';
import { loadGLTFModel } from '../../lib/model';
import styles from './ThreeDView.module.scss';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { cloudFrontUrl } from '../../utils/constants';
import { animationGenrator } from '../../utils/assets';
import { Modal } from '@mui/material';
import { useIsMobile } from '../../hooks/useIsMobile';
import { useIsDesktop } from '../../hooks/useIsDesktop';

const ThreeDView = () => {
  const [isModal, setIsModal] = useState(false);
  const [isMobile] = useIsMobile();
  const [isDesktop] = useIsDesktop();

  useEffect(() => {
    renderVilla();
  }, []);
  const renderVilla = () => {
    // Canvas
    const canvas = document.querySelector('#villa');
    if (canvas) {
      // Scene
      const scene = new THREE.Scene();

      // GLTF Loader
      loadGLTFModel(scene, '/models/house2.gltf', {
        receiveShadow: false,
        castShadow: false,
      });

      // Lights
      const ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
      const pointLight = new THREE.PointLight(0xffffff, 1);
      pointLight.position.x = 0;
      pointLight.position.y = 2;
      pointLight.position.z = 0;
      scene.add(ambientLight, pointLight);

      /**
       * Sizes
       */
      const sizes = {
        width: window.innerWidth,
        height: window.innerHeight,
      };

      // Cursor
      const cursor = {
        x: 0,
        y: 0,
      };

      window.addEventListener('mousemove', (event) => {
        cursor.x = event.clientX / sizes.width - 0.5;
        cursor.y = -(event.clientY / sizes.height - 0.5);
      });

      window.addEventListener('resize', () => {
        // Update sizes
        sizes.width = window.innerWidth;
        sizes.height = window.innerHeight;

        // Update camera
        camera.aspect = sizes.width / sizes.height;
        camera.updateProjectionMatrix();

        // Update renderer
        renderer.setSize(sizes.width, sizes.height);
        renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
      });

      /**
       * Camera
       */
      // const camera = new THREE.PerspectiveCamera(
      //   75,
      //   sizes.width / sizes.height,
      //   0.1,
      //   10000
      // );
      const camera = new THREE.OrthographicCamera(
        sizes.width / -60,
        sizes.width / 60,
        sizes.height / 60,
        sizes.height / -60,
        1,
        10000
      );
      camera.position.x = 0;
      camera.position.y = 5;
      camera.position.z = 20;

      // scene.add(camera);

      /**
       * Renderer
       */
      const renderer = new THREE.WebGLRenderer({
        canvas: canvas,
        alpha: true,
      });
      renderer.setSize(sizes.width, sizes.height);
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

      // Controls
      const controls = new OrbitControls(camera, renderer.domElement);
      controls.enableDamping = true;
      controls.enableZoom = false;

      // Clock
      const clock = new THREE.Clock();

      const tick = () => {
        const elapsedTime = clock.getElapsedTime();

        // Render
        renderer.render(scene, camera);

        window.requestAnimationFrame(tick);
      };

      tick();
    }
  };

  useEffect(() => {
    setTimeout(() => {
      animationGenrator('.threed-text', '#threedSection');
      // animationGenrator(".modalImg", "#modalImg");
    }, 1000);
  }, []);

  useEffect(() => {
    if (isModal) {
      animationGenrator('.modalImg', '#modalImg');
    }
  }, [isModal]);

  return (
    <div className={styles.wrapper}>
      <div
        style={{
          backgroundImage: `url(${cloudFrontUrl}/second-fold-img.webp)`,
        }}
        className="second-fold-img"
        data-aos="fade-up"
        data-aos-duration="1000"
      >
        <div className="tag"> Lifestyle Image </div>
      </div>

      <section
        id="threedSection"
        className={`${styles['third-section']} animation-block`}
      >
        <div>
          <div className={styles['text-div']}>
            <div className={styles['title']}>
              <div className="threed-text">Make Memories</div>
              <div className="threed-text">
                <span>on {!isDesktop && 'your'}</span>
                {isDesktop && 'Your'} own land
              </div>
            </div>
            <div className={`${styles['content']} threed-text`}>
              Millgrove extends an unparalleled offering to a select few - a
              rare chance to own a vast plot in prime Gurugram. Every plot for
              sale is in excess of 1100 square yards, providing ample space on
              which to build an exquisite independent home. The estate is
              ultra-low-density by design with only a limited number of these
              large plots thoughtfully arranged over 23 acres.
            </div>
          </div>
          <div
            className={`${styles['explore-btn-mobile']} threed-text`}
            onClick={() => setIsModal(true)}
          >
            explore masterplan
          </div>
          <div className={`${styles['potential-image']} threed-text`}>
            <img
              src={`${cloudFrontUrl}/dad_swinging_daughter.webp`}
              alt="potential-image"
              loading="lazy"
            />
            <div className="tag md grey"> Lifestyle Image </div>
          </div>
        </div>
        <div
          className={`${styles['explore-btn']} threed-text`}
          onClick={() => setIsModal(true)}
        >
          explore masterplan
        </div>
      </section>
      <div
        className="parallax"
        style={{
          backgroundImage: `url(${cloudFrontUrl}/semi-aerial.webp)`,
          display: isMobile ? 'none' : 'block',
        }}
      >
        <div className="tag"> Artistic Impression </div>
      </div>

      <Modal open={isModal} onClose={() => setIsModal(false)}>
        <div className={styles['modal-container']}>
          <div
            className={`${styles['modal-img']} animation-block`}
            id="modalImg"
            data-aos="fade-up"
            data-aos-duration="1000"
          >
            <img
              src={`${cloudFrontUrl}/masterplan.webp`}
              alt="modal-img"
              width={'100%'}
              height={'100%'}
              className="modalImg"
            />
            <div className={styles['cross']} onClick={() => setIsModal(false)}>
              <img src="/images/cross.svg" alt="cross" />
            </div>
            <div className="tag"> Artistic Impression </div>
          </div>
          <div className="warning-container portrait">
            <img width="200px" src="/images/portrait-rotate.webp" />
            <p>Please rotate your device</p>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ThreeDView;
