import React from 'react';
import { useRouter } from 'next/router';
import { CloseIcon } from '../../public/icons/icons';
import styles from './cookies.module.scss';

const Cookies = () => {
  const router = useRouter();
  const isComingFromEmail = router.query.fromEmail;

  return (
    <div className={styles.pageWrapper}>
      <div className={styles.cookiesWrapper}>
        <div className={styles.content}>
          <h2>Cookies Policy</h2>
          <p>
            This Cookies Policy supplements our Privacy Policy and describes the
            different types of cookies and other similar technologies that may
            be used in connection with the Platform to facilitate the Services
            to You. This Cookies Policy also describes how you can manage
            cookies and other similar technologies. Please read this Cookies
            Policy with our Privacy Policy.
          </p>
          <p>
            Capitalized terms used but not defined in this Cookies Policy can be
            found in our Privacy Policy, Terms of Service, and any other rules,
            policies or guidelines published on the Platform as applicable to
            you (collectively referred to as the "Platform terms")
          </p>
          <p>
            By accessing our Platform, you agree to the use of cookies and
            similar technologies for the purposes described in this Cookies
            Policy.
          </p>
          <strong>What are Cookies and other similar technologies?</strong>
          <p>
            Cookies are small files of data in the form of text files created by
            web browsers you may use and is stored on your device’s hard drive.
            They allow website providers to identify users, store preferential
            information, and understand users’ browsing activities.
          </p>
          <strong>
            What Cookies and Other Technologies Are in Use and Why Do We Use
            Them?
          </strong>
          <ul>
            <li>
              <p>
                <strong>Essential Cookies:</strong> Essential Cookies are
                necessary for the Platform to work and enable you to move around
                it and use its services and features. Disabling these cookies
                may make the services and features unavailable.
              </p>
            </li>
            <li>
              <p>
                <strong>Functional Cookies:</strong> We use Functional Cookies
                to save your settings on the Platform—settings such as language
                and other user preferences you have previously opted for on the
                Site. We may also use "Flash Cookies" for some of our animated
                or other video content.
              </p>
            </li>
            <li>
              <p>
                <strong>Session Cookies:</strong> These types of cookies are
                stored only temporarily during a browsing session and are
                deleted from your device when you close the browser or the
                application. We use Session Cookies to support the functionality
                of the Platform and to understand your use of the Platform—that
                is, which pages you visit, which links you use, what content you
                are viewing, and how long you stay on each page.
              </p>
            </li>
          </ul>
        </div>
        {!isComingFromEmail ? (
          <div
            onClick={() => router.back()}
            className={styles.closeIconWrapper}
          >
            <CloseIcon color="#f9f6ed" height="100%" width="100%" />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Cookies;
