import { cloudFrontUrl } from './constants';

const timelineData = [
  {
    year: '1992',
    description:
      'First parcel of land purchased, and a foundation stone for a rice mill was laid during the Bhumi Pujan by our founders, Narshidas and Hiraben Thakrar; affectionately known as Bhai and Mataji.',
    media: `${cloudFrontUrl}/1992-new.webp`,
    imgTitle: 'Actual Photo'
  },
  {
    year: '1996',
    description:
      'Bhai and Mataji sign the World Bank Group finance agreement for the construction of a rice mill in Gurugram.',
    media: `${cloudFrontUrl}/1996-new.webp`,
    imgTitle: 'Actual Photo'
  },
  {
    year: '2008',
    description:
      'State-of-the-art rice mill is inaugurated in Sector 76, Gurugram.',
    media: `${cloudFrontUrl}/2008-small.webp`,
    imgTitle: 'Actual Photo'
  },
  {
    year: '2014',
    description:
      'The rice business is sold but the land is retained by the founding family for the future development of Millgrove.',
    media: `${cloudFrontUrl}/2014-plain.webp`,
    imgTitle: 'Actual Photo'
  },
  {
    year: '2017',
    description:
      'The rice mill is demolished after obtaining a development licence for a residential plotted colony.',
    media: `${cloudFrontUrl}/conclusion.mp4`,
  },
  {
    year: '2020',
    description:
      'Millgrove ground is broken, and the foundation stone is laid during the havan ceremony. Civil infrastructure development commences.',
    media: `${cloudFrontUrl}/2020-new.webp`,
    imgTitle: 'Actual Photo'
  },
  // {
  //   year: '2021',
  //   description: 'Millgrove home construction begins on plots 18, 19 & 20.',
  //   media: `${cloudFrontUrl}/2021-small.webp`,
  // },
  {
    year: '2022',
    description:
      'Millgrove licence completion. All civil infrastructure, services and plot demarcation is fully executed.',
    media: `${cloudFrontUrl}/construction-begins.mp4`,
  },
];

export { timelineData };
