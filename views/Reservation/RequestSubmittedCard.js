import React from 'react';
import { MILLGROVE_GRADIENT_LEAF } from '../../utils/assets';
import styles from './Reservation.module.scss';

const RequestSubmittedCard = () => {
  return (
    <div className={styles.formWrapper}>
      <div className={styles.formBg}>
        <div className={styles.zIndexStyle}>
          <div className={styles.requestSubmited}>
            <h1 className={`${styles.formHeading} ${styles.requestHeading}`}>
              <MILLGROVE_GRADIENT_LEAF className={styles.gradientLeaf} />
              VISIT REQUESTED
            </h1>
            <p className={`${styles.requestDescription}`}>
              A member of our dedicated team will reach out soon to schedule a
              time that is convenient for you.
            </p>
          </div>
          <div className={`${styles.millgroveText}`}>Millgrove</div>
        </div>
      </div>
    </div>
  );
};

export default RequestSubmittedCard;
