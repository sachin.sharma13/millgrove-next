import { motion } from 'framer-motion';
import { useRouter } from 'next/router';

const AnimatedLayout = ({ children }) => {
  const router = useRouter();
  return (
    <motion.div
      key={router.route}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ ease: 'easeIn', duration: 2 }}
    >
      {children}
    </motion.div>
  );
};
export default AnimatedLayout;
