import { useRouter } from 'next/router';
import React from 'react';
import { CloseIcon } from '../../public/icons/icons';
import styles from './privacy.module.scss';

const PrivacyPolicy = () => {
  const router = useRouter();
  const isComingFromEmail = router.query.fromEmail;

  return (
    <div className={styles.pageWrapper}>
      <div className={styles.cookiesWrapper}>
        <div className={styles.content}>
          <h2 id="how-does-Millgrove-use-my-information">Privacy Policy</h2>
          <p>
            TRL Riceland Private Limited (“Company”) is committed to maintain
            the privacy and accuracy of information that has been provided on
            this website.
          </p>
          <p>
            By using this website, you agree to the collection and use of
            information in accordance with this Privacy Policy as may be
            modified from time to time. Users are advised not to access this
            site if they do not agree to this Privacy Policy. This website does
            not constitute part of a legal contract.
          </p>
          <p>
            This Privacy Policy covers the Company’s usage of personally
            identifiable material that is shared by you on this website. This
            Policy also covers treatment of any personally identifiable
            information shared with the Company.
          </p>
          <p>
            This Privacy Policy does not apply to the activities of
            organizations not owned or controlled by the Company, or to people
            who are not employed or managed by the Company and also in respect
            of information required to be shared with public/private
            authority(ies) by operation of any applicable law, court order or
            direction.
          </p>
          <p>
            <strong>Collection and Usage of Information</strong>
          </p>
          <ul>
            <li>
              We use your personal information only for providing and improving
              the website
            </li>
            <li>
              We compile personally identifiable information when you submit any
              form on this website.
            </li>
            <li>
              While using this website, we may ask you to provide us with
              certain personally identifiable information that can be used to
              contact or identify you. Personally identifiable information may
              include but is not limited to your name, email address and contact
              number.
            </li>
            <li>
              Once a form is submitted and it reaches us, the information
              provided by you is saved in our database and is not shared with
              any third party.
            </li>
            <li>
              We use this information to revert to your query and future
              reference, and to contact you about products & services.
            </li>
            <li>
              Like many website operators, we collect information that your
              browser sends whenever you visit our website. This log data may
              include information such as your computer's internet protocol, IP
              address, browser type, browser version, mobile device information,
              location, network carrier, the pages of our website that you
              visit, the time and date of your visit, the time spent on those
              pages and other statistics.
            </li>
          </ul>
          <p>
            <strong>The following information is collected:</strong>
          </p>
          <ul>
            <li>
              Your personal information including but not limited to your name,
              email address and contact number
            </li>
            <li>
              A record of any interaction and correspondence between you and the
              Company.
            </li>
            <li>
              Your responses to customer surveys conducted by the Company or on
              their behalf.
            </li>
            <li>
              Details of your visits to this website and other information
              collected through cookies and other tracking technology including
              information that you look at.
            </li>
            <li>Online Identifiers.</li>
          </ul>
          <p>
            <strong>Sharing and Disclosure of Information:</strong>
          </p>
          <ul>
            <li>
              We do not sell, rent, or share personally identifiable information
              made available to the Company.
            </li>
            <li>
              We share personally identifiable information to other companies or
              people only if:
              <ol>
                <li>
                  We have consent to share the information; or we may disclose
                  personal information if required to do so by law or in the
                  good faith belief that such disclosure is reasonably necessary
                  to respond to court orders/summons/directions, or other legal
                  process.
                </li>
                <li>
                  We may disclose personal information to law enforcement
                  offices, third party rights owners, or others in the good
                  faith belief that such disclosure is reasonably necessary to:
                  enforce our Terms of Privacy Policy; respond to claims that an
                  advertisement, posting, or other content violates the rights
                  of a third party; or protect the rights, property or personal
                  safety of our users or the general public.
                </li>
                <li>
                  We find that your actions on our website violate our{' '}
                  <span
                    onClick={() => router.push('/terms-and-conditions')}
                    className={styles['external-link']}
                  >
                    Terms and Conditions
                  </span>
                  .
                </li>
              </ol>
            </li>
          </ul>
          <p>
            The content and design of the website are the exclusive property of
            the Company. No person can use or reproduce or modify or allow any
            other to use or reproduce the content or image or logo used on this
            website for any reason without prior written consent from the
            Company. The use of content or image or logo used in this website
            without permission is prohibited.
          </p>
          <p>
            The Privacy Policy is subject to change and any modification will be
            communicated here. The user(s) hereby acknowledge and agree that it
            is their responsibility to review this Privacy Policy periodically
            and become aware of modifications.
          </p>
          <p>
            Appropriate measures have been taken to ensure that user credentials
            are not misused, accidentally destroyed, or lost within the
            environment of the Company. Though care has been taken in the
            production of information on this website, the Company will not be
            responsible and accept any liability for loss incurred in anyway
            whatsoever, by anyone who may seek to depend on the information
            contained herein. The Company shall not be responsible for any
            actions of any third parties or events that are beyond our
            reasonable control including but not limited to acts of government,
            computer hacking, unauthorized access to computer data and storage
            device and computer crashes etc.
          </p>
          <p>
            This website may use “cookies” to enhance user experience. Web
            browsers place cookies on hard drives for record-keeping purposes
            and sometimes to track information about their users. Users may
            choose to set their web browser to refuse cookies or to alert them
            when cookies are being sent. If they do so, note that some parts of
            the website may not function properly.
          </p>
          <p>
            If you wish that we no longer use your personal information in
            accordance with the terms of this privacy policy, contact us at
            <strong> info@millgrove.in</strong> .{' '}
          </p>
          <p>
            <strong>Grievance Officer</strong>
          </p>
          <p>
            The name and contact details of the Grievance Officer are provided
            below:
          </p>
          <p>Mr. Tommy Thomas</p>
          <p>TRL Riceland Private Limited </p>
          <p>42km Stone, NH-48, Sector- 76, Gurugram, Haryana 122004, India</p>
          <p>Phone: +91 124 470 4600 </p>
          <p>
            Email: <strong>thomas@trlriceland.com</strong>{' '}
          </p>
          <p>
            If you have any questions about this policy or other privacy
            concerns, you can also email us at
            <strong> info@millgrove.in</strong>.{' '}
          </p>
          <p>
            <strong>Jurisdiction</strong>
          </p>
          <p>
            If you choose to visit our website, your visit and any dispute over
            privacy is subject to this Policy and the website's{' '}
            <span onClick={() => router.push('/terms-and-conditions')}>
              Terms & Conditions{' '}
            </span>{' '}
            . In addition to the foregoing, any disputes arising under this
            Policy shall be governed by the laws of India and exclusive
            jurisdiction of RERA authority and/or Courts at Gurugram, Haryana
            and High Court of Punjab and Haryana, India.
          </p>
        </div>
        {!isComingFromEmail ? (
          <div
            onClick={() => router.back()}
            className={styles.closeIconWrapper}
          >
            <CloseIcon color="#f9f6ed" height="100%" width="100%" />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default PrivacyPolicy;
