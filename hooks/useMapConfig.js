import { useEffect, useState } from 'react';

const useMapConfig = () => {
  let centerCoordinates = [77.08922259235733, 28.533052374693224];

  let mapZoomLevel = 11.2;

  if (typeof window === null) return;

  if (window.matchMedia('(max-width: 1024px)').matches) {
    centerCoordinates = [77.08922259235733, 28.493052374693224];
    mapZoomLevel = 10.7;
  }

  if (window.matchMedia('(max-width: 600px)').matches) {
    mapZoomLevel = 9.5;
  }
  return [centerCoordinates, mapZoomLevel];
};

export { useMapConfig };
