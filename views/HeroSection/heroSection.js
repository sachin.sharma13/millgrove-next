import gsap from 'gsap';
import { useEffect, useRef, useState } from 'react';
import { useIsMobile } from '../../hooks/useIsMobile';
import { useRouter } from 'next/router';
import { useIntersection } from '../../hooks/useIntersection';
import { animationGenrator } from '../../utils/assets';
import { useLayoutEffect } from 'react';

/* eslint-disable @next/next/no-img-element */
function HeroSection() {
  const btnRef = useRef();
  const [isMobile] = useIsMobile();
  const router = useRouter();

  useLayoutEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
    animationGenrator('.animation-home-text', '#heroSection');
  }, []);

  useEffect(() => {
    if (btnRef) {
      setTimeout(() => {
        btnRef?.current?.classList.add('springAction');
      }, 3000);
    } else {
      btnRef?.current?.classList.remove('springAction');
    }
    return () => btnRef?.current?.classList.remove('springAction');
  }, []);

  return (
    <div id="heroSection" className="hero-section-fold">
      <div className="mg-first-fold">
        <div className="container">
          <div className="mg-first-fold-text">
            <div>
              <div className="mg-first-fold-heading animation-block">
                <div className="animation-home-text">MEANINGFUL</div>
                <div className="animation-home-text">LIVING</div>
              </div>
              <div className="animation-block">
                <div className="mg-first-fold-subheading animation-home-text">
                  Living at Millgrove transcends ordinary notions of luxury. It
                  is a natural haven that gives you a complete sense of
                  belonging. A place where you can nurture relationships and
                  make time for what matters most.
                </div>
              </div>
            </div>
          </div>
          <div
            onClick={() => {
              isMobile
                ? window.scrollTo({
                    top: 1700,
                    behavior: 'smooth',
                  })
                : router.push('/home#madeWithCare ');
            }}
            className="more-button"
          >
            <div ref={btnRef} className="scroll-down">
              <img src="/images/arrow-down.svg" alt="" />
            </div>
            Discover more
          </div>
        </div>
        <div className="mg-bg-video">
          <video
            playsInline
            autoPlay
            muted
            loop
            preload="none"
            className="lazy"
          >
            <source
              src={`${process.env.NEXT_PUBLIC_CLOUDFRONT_URL}/backgroundVideo.mp4`}
              type="video/mp4"
            />
            Your browser does not support the video tag.
          </video>
        </div>
        <div className="tag bottom green"> Artistic Impression </div>
      </div>
    </div>
  );
}

export default HeroSection;
