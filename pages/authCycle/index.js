import { useContext, useEffect, useRef, useState } from 'react';
import Firstfold from '../../views/FirstFold/firstfold';
import Home from '../../views/Home/home';
//Components
import CookiesPopup from '../../components/CookiesPopup/cookiesPopup';
import Login from '../../views/Login/Login';
import Signup from '../../views/Signup/Signup';
import SignupSuccess from '../../views/Signup/SignupSuccess';
import { AuthContext } from '../../context/AuthContext';
import { useInitialRender } from '../../hooks/useInitialRender';

const AuthCycle = () => {
  const { hasLoaderScreenLoadedOnce, setHasLoaderScreenLoadedOnce } =
    useContext(AuthContext);
  const [menu, setMenu] = useState(false);
  const [cookiesPopup, setCookiesPopup] = useState(false);
  const [isRegistering, setIsRegistering] = useState(false);
  const [isRegisterationSuccessfull, setIsRegisterationSuccessfull] =
    useState(false);
  const [isLoggingIn, setIsLoggingIn] = useState(false);
  const [isInitialRender] = useInitialRender();

  useEffect(() => {
    setTimeout(() => {
      if (!hasLoaderScreenLoadedOnce) {
        setHasLoaderScreenLoadedOnce(true);
      }
    }, 5000);
  }, []);

  return (
    <div className="login-process">
      <div className="main-components">
        {cookiesPopup && (
          <CookiesPopup closeCookiesPopup={() => setCookiesPopup(false)} />
        )}

        {!hasLoaderScreenLoadedOnce && <Home />}

        {!isRegistering && !isRegisterationSuccessfull && !isLoggingIn && (
          <Firstfold
            openMenu={() => setMenu(true)}
            setIsLoggingIn={setIsLoggingIn}
            setIsRegistering={setIsRegistering}
            isInitialRender={isInitialRender}
          />
        )}

        {isRegistering && (
          <Signup
            setIsLoggingIn={setIsLoggingIn}
            isRegistering={isRegistering}
            setIsRegistering={setIsRegistering}
            isRegisterationSuccessfull={isRegisterationSuccessfull}
            setIsRegisterationSuccessfull={setIsRegisterationSuccessfull}
          />
        )}

        {isRegisterationSuccessfull && <SignupSuccess />}
        {isLoggingIn && (
          <Login
            setIsLoggingIn={setIsLoggingIn}
            setIsRegistering={setIsRegistering}
          />
        )}
      </div>

      <div className="login-video-bg">
        <video playsInline autoPlay muted loop preload="none" className="lazy">
          <source
            src={`${process.env.NEXT_PUBLIC_CLOUDFRONT_URL}/backgroundVideo.mp4`}
            type="video/mp4"
          />
          Your browser does not support the video tag.
        </video>
      </div>
    </div>
  );
};

export default AuthCycle;
