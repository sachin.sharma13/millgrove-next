import Link from 'next/link';
import React from 'react';
import styles from './Layout.module.scss';
import { useRouter } from 'next/router';

const Layout = ({ children, layoutStyle, footerHide }) => {
  const router = useRouter();
  return (
    <div className={`${styles.wrapper} ${layoutStyle}`}>
      <div className={styles.mainContentWrapper}>
        {/* <Header /> */}
        <div className={styles.childrenWrapper}>{children}</div>
      </div>

      {!footerHide && (
        <footer className={styles.footerCustom}>
          <div className="left-footer-links">
            <a
              onClick={() => router.push('/privacy-policy')}
              className="hover-underline-animation"
            >
              Privacy Policy
            </a>
            <a
              onClick={() => router.push('/terms-and-conditions')}
              className="hover-underline-animation"
            >
              Terms & Conditions
            </a>
          </div>
        </footer>
      )}
    </div>
  );
};

export default Layout;
