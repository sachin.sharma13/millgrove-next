import React, { useContext, useEffect, useRef, useState } from 'react';

// Views Sections here
import HeroSection from '../../views/HeroSection/heroSection';
import HomeCarousel from '../../views/HomeCarousel/HomeCarousel';
import HomeCarouselMobile from '../../views/HomeCarouselMobile/HomeCarouselMobile';
import Reservation from '../../views/Reservation';
import Secondfold from '../../views/SecondFold/secondfold';
import ThreeDView from '../../views/ThreeDView';
import Layout from '../../components/Layout/Layout';
import Footer from '../../views/Footer/Footer';
import { AuthContext } from '../../context/AuthContext';
import { useRouter } from 'next/router';
import Header from '../../components/Header/Header';
import AnimatedLayout from '../../components/AnimatedLayout/AnimatedLayout';
import { useIntersection } from '../../hooks/useIntersection';
import { useIsMobile } from '../../hooks/useIsMobile';
import gsap from 'gsap';

export default function Home() {
  const { userToken } = useContext(AuthContext);
  const router = useRouter();
  const mobileBtnRef = useRef();
  const isMobileBtnVisible = useIntersection(mobileBtnRef);
  const btnRef = useRef();
  const isVisible = useIntersection(btnRef);
  const [isMobile] = useIsMobile();

  useEffect(() => {
    // foll line is added, coz token from context comes null for one render and causes a momentary flash of loading bar
    const userTokenFromStorage = JSON.parse(
      localStorage.getItem('userToken')
    )?.authToken;
    if (!userToken && !userTokenFromStorage) {
      router.push('/');
    }
  }, [userToken]);

  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
  }, []);

  useEffect(() => {
    if (!isMobile) return;

    let controller = new ScrollMagic.Controller();

    const wipeAnimation = gsap.to('div.hero-section-fold', {
      y: '-93%',
      duration: 1,
      ease: Quad.easeInOut,
    }); // in from top

    // create scene to pin and link animation
    new ScrollMagic.Scene({
      triggerElement: '#pinContainer',
      triggerHook: 'onLeave',
      duration: '200%',
    })
      .setPin('#pinContainer')
      .setTween(wipeAnimation)
      .addTo(controller);
  }, [isMobile]);

  return (
    <>
      <AnimatedLayout>
        <Header
          isLandingPage={true}
          isFooterVisible={isMobileBtnVisible || isVisible}
        />
        <Layout footerHide={true}>
          <div className="home-section">
            {isMobile ? (
              <div id="pinContainer">
                <Secondfold />
                <HeroSection />
              </div>
            ) : (
              <>
                <HeroSection />
                <Secondfold />
              </>
            )}
            <ThreeDView />
            <HomeCarousel />
            <HomeCarouselMobile />
            <Reservation
              mobileBtnRef={mobileBtnRef}
              isMobileBtnVisible={isMobileBtnVisible}
              btnRef={btnRef}
              isVisible={isVisible}
            />
            <Footer />
            <div className="warning-container landscape">
              <img src="/images/landscape-rotate.webp" />
              <p>Please rotate your device</p>
            </div>
          </div>
        </Layout>
      </AnimatedLayout>
    </>
  );
}
