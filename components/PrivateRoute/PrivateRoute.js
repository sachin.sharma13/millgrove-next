import { useRouter } from 'next/router';
import React, { useContext, useEffect } from 'react';
import { AuthContext } from '../../context/AuthContext';

const PrivateRoute = ({ children }) => {
  const { userToken } = useContext(AuthContext);
  const router = useRouter();
  useEffect(() => {
    if (!userToken) {
      return router.push('/');
    }
  }, [userToken]);
  return <div>{children}</div>;
};

export default PrivateRoute;
