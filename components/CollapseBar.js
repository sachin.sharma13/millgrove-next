import { useEffect, useRef, useState } from 'react';
import { useMapConfig } from '../hooks/useMapConfig';

const CollapseBar = ({
  styles,
  type,
  index,
  storeTypes,
  map,
  currentlyOpen,
  setCurrentlyOpen,
}) => {
  const [isExpanded, setExpanded] = useState(false);
  const [currentPopup, setCurrentPopup] = useState(null);
  const timeoutRef = useRef(null);
  const [centerCoordinates, mapZoomLevel] = useMapConfig();

  const storesHandler = (zoom = false, showAll = false) => {
    Object.keys(storeTypes).forEach((item) => {
      if (showAll) {
        map?.getSource(item?.split(' ').join('').toLowerCase()).setData({
          type: 'FeatureCollection',
          features: storeTypes[item],
        });
        return;
      }
      if (item === type) {
        map?.getSource(item?.split(' ').join('').toLowerCase()).setData({
          type: 'FeatureCollection',
          features: storeTypes[item],
        });
        return;
      }
      map?.getSource(item?.split(' ').join('').toLowerCase()).setData({
        type: 'FeatureCollection',
        features: [],
      });
    });
    zoom &&
      map?.flyTo({
        center: centerCoordinates,
        zoom: mapZoomLevel,
        bearing: 300,
        duration: 2000,
        easing: (t) => {
          return t * (2 - t);
        },
      });
  };

  useEffect(() => {
    if (index === currentlyOpen) {
      setExpanded(true);
    } else {
      setExpanded(false);
    }
  }, [currentlyOpen]);

  const handleCollapse = (type = false) => {
    setExpanded((prev) => {
      if (prev) {
        setCurrentlyOpen(null);
        storesHandler(true, !type);
      } else {
        setCurrentlyOpen(index);
        storesHandler(true);
      }
      return !prev;
    });
  };

  useEffect(() => {
    setTimeout(() => {
      storesHandler(true, true);
    }, 5000);
  }, []);

  const showPopup = (item) => {
    const popup = new mapboxgl.Popup();
    setCurrentPopup(popup);

    const coordinates = item.geometry.coordinates;
    const description = item.properties.name;
    const driveTime = item.properties.driveTime;

    try {
      popup
        ?.setLngLat(coordinates)
        ?.setHTML(
          '<h6>' +
            description +
            '</h6>' +
            '\n' +
            '<h6>' +
            '<img src="/images/popup-car.png" alt="car" height="20px"/>' +
            ' ' +
            driveTime +
            '</h6>'
        )
        .addTo(map);
    } catch (e) {}
  };

  const closePopup = () => {
    currentPopup?.remove();
  };

  return (
    <div key={index} className={styles['collapse-bar']}>
      <div className={styles['header']}>
        <img
          className={styles['title-img']}
          src={`/images/${type.split(' ').join('').toLowerCase()}-icon.png`}
          alt="title-icon"
        />
        <div
          onMouseEnter={() => {
            if (index === currentlyOpen || currentlyOpen !== null) return;
            storesHandler(false);
          }}
          onMouseLeave={() => {
            if (index === currentlyOpen || currentlyOpen !== null) return;
            storesHandler(false, true);
          }}
          onClick={() => handleCollapse(true)}
        >
          {type}
        </div>
        <img
          style={
            isExpanded
              ? { transform: 'rotate(0deg)', marginLeft: 'auto' }
              : { transform: 'rotate(180deg)', marginLeft: 'auto' }
          }
          onClick={() => handleCollapse()}
          src={`${process.env.NEXT_PUBLIC_CLOUDFRONT_URL}/Vector.svg`}
          alt="collapse-icon"
        />
      </div>
      <div
        style={isExpanded ? { display: 'flex' } : { display: 'none' }}
        className={styles[`content`]}
      >
        {storeTypes[type].map((item, index) => (
          <span
            key={index}
            onMouseEnter={() => showPopup(item)}
            onMouseLeave={() => {
              closePopup();
              clearTimeout(timeoutRef.current);
            }}
            onClick={() => {
              closePopup();
              map.flyTo({
                center: item.geometry.coordinates,
                zoom: mapZoomLevel,
                duration: 2000,
              });
              timeoutRef.current = setTimeout(() => {
                showPopup(item);
              }, 2000);
            }}
          >
            {item.properties.name}
          </span>
        ))}
      </div>
    </div>
  );
};

export default CollapseBar;
