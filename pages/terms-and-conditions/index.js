import React from 'react';
import TnC from '../../views/TermsAndConditions/TermsAndConditions';

const TermsAndConditions = () => {
  return <TnC />;
};

export default TermsAndConditions;
